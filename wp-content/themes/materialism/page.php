<?php
get_header();
$page_id = tt_get_page_id();
$sidebar = get_post_meta($page_id, THEME_NAME . '_sidebar_position', true);
$hide_header = get_post_meta($page_id, THEME_NAME . '_hide_header', true);
$single_post_shortcodes = get_post_meta( $page_id, THEME_NAME . '_single_post_shortcodes', true );
$sidebar_status = is_active_sidebar('main-sidebar');
if(empty($sidebar)) : $sidebar = 'full_width'; endif;
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php if($hide_header === 'normal_header' || $hide_header === '') : ?>
		<section class="hero-section single" style="<?php if(has_post_thumbnail()) echo "background: url('". get_the_post_thumbnail_url( $post ,'tt_big_post_thumbnail') ."') center center;"; else echo "background: #242433;"; ?>">
			<div class="container">
				<div class="centered-content">
					<h1 class="post-title"><?php the_title(); ?></h1>
				</div>
				<div class="single-share-block">
		    		<span class="circle-btn share-btn"></span>
		    		<?php tt_share(); ?>
		    	</div>
			</div>
		</section>
	<?php endif; ?>

<?php if(!has_shortcode(get_post_field( 'post_content', $page_id),'vc_row') || ($sidebar !== 'full_width' && has_shortcode(get_the_content(),'vc_row'))) : ?>
	<section class="single-blog-post">
		<div class="container <?php if($sidebar === 'right' && $sidebar_status) echo 'has-sidebar'; elseif ($sidebar === 'left') echo 'has-sidebar-left'; ?>">
			<?php if('full_width' !== $sidebar && $sidebar_status) : ?>
				<div class="row">
			<?php endif; ?>
<?php endif; ?>
				<?php if($sidebar === 'left' && $sidebar_status) : ?>
					<div class="col-md-4">
						<?php get_sidebar() ?>
					</div>
				<?php endif; ?>
<?php if(!has_shortcode(get_post_field( 'post_content', $page_id),'vc_row') || ($sidebar !== 'full_width' && has_shortcode(get_the_content(),'vc_row'))) : ?>
				<?php if( $sidebar !== 'full_width' && $sidebar_status ) print '<div class="col-md-8">'; else print '<div class="col-md-10 col-md-offset-1">';  ?>
					<div class="description">
<?php endif; ?>

						<?php the_content(); ?>


<?php if(!has_shortcode(get_post_field( 'post_content', $page_id),'vc_row') || ($sidebar !== 'full_width' && has_shortcode(get_the_content(),'vc_row'))) : ?>
					</div>
					<?php comments_template(); ?>
				<?php if( $sidebar_status ) print '</div>'; ?>
<?php endif ?>
				<?php if($sidebar === 'right' && $sidebar_status) : ?>
					<div class="col-md-4">
						<?php get_sidebar() ?>
					</div>
				<?php endif; ?>
<?php if(!has_shortcode(get_post_field( 'post_content', $page_id),'vc_row') || ($sidebar !== 'full_width' && has_shortcode(get_the_content(),'vc_row'))) : ?>
			<?php if('full_width' !== $sidebar && $sidebar_status) : ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<?php endwhile; endif;

if($single_post_shortcodes) :
	$shortcodes_list = str_replace(', ', ',', $single_post_shortcodes); 
	$shortcodes = explode(",", $shortcodes_list); 
	foreach ($shortcodes as $shortcod) :
		print do_shortcode( $shortcod );
	endforeach;
endif;

 get_footer(); ?>