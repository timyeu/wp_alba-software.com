<?php
$post_slug = $post_style = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$sg_post = new WP_Query( array( 'name' => $post_slug ) );


if($sg_post->have_posts()) : while($sg_post->have_posts()) : $sg_post->the_post();

	$rest = substr(get_the_excerpt(), 0 , 145).' ...';

	if($post_style === 'style_1') : ?>
			<div class="news-item">
				<span class="category"><?php the_category(' '); ?></span>
				<h3 class="title"><?php the_title(); ?></h3>
				<p class="description"><?php tt_print($rest); ?></p>
				<a href="<?php the_permalink() ?>" class="btn white readmore"><?php esc_html_e('read more', 'materialism') ?></a>
			</div>
	<?php elseif($post_style === 'style_2') : ?>
			<div class="news-item-cover">
				<div class="cover">
					<?php the_post_thumbnail(); ?>
					<span class="category"><?php the_category(' '); ?></span>
				</div>
				
				<h3 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				<p class="description"><?php tt_print($rest); ?></p>
			</div>
	<?php elseif($post_style === 'style_3') : ?>
			<div class="news-item-cover big clearfix">
				<div class="content">
					<span class="category"><?php the_category(' '); ?></span>
					<h3 class="title"><?php the_title(); ?></h3>
					<p class="description"><?php tt_print($rest); ?></p>
					<a href="<?php the_permalink() ?>" class="btn readmore"><?php esc_html_e('read more', 'materialism') ?></a>
				</div>

				<div class="cover">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
	<?php endif; 
endwhile; endif; ?>
