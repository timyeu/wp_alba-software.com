<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$hero_back = wp_get_attachment_image_src($hero_background, 'original') ? wp_get_attachment_image_src($hero_background, 'original') : '#242433';
$brand = wp_get_attachment_image_src($hero_brand, 'original') ? wp_get_attachment_image_src($hero_brand, 'original') : '';
$hero_button_text = $hero_button_text ? $hero_button_text : 'Learn more';
$hero_button_link = $hero_button_link ? $hero_button_link : '#';
?>
<section  class="hero-section section" style="background: <?php tt_print('url('.$hero_back[0].');'); if($hero_background) echo ' background-size: cover;'?>">
	<div class="centered-content<?php tt_print($css_class) ?>">
		<?php if(!$hide_hero_brand) : ?>
			<img src="<?php tt_print($brand[0]) ?>" alt="hero logo">
		<?php endif; ?>
		<h1 class="hero-title"><?php tt_print($hero_title) ?></h1>
		<p class="description"><?php tt_print($hero_subtitle) ?></p>
		<?php if(!$hide_hero_button) : ?>
			<a class="btn big gradient" href="<?php tt_print($hero_button_link) ?>"><?php tt_print($hero_button_text) ?></a>
		<?php endif; ?>
	</div>
	<?php if(!$hide_hero_scroll_button) : ?>
		<button class="circle-btn go-down"></button>
	<?php endif; ?>
</section>