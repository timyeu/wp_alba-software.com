<?php
$subs_title = $subs_text = $subs_button_text = $subs_placeholder = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$subs_title = $subs_title ? $subs_title : 'Subscribe';
$subs_button_text = $subs_button_text ? $subs_button_text : 'subscribe now';
$subs_placeholder = $subs_placeholder ? $subs_placeholder : 'Email Address';
$bg_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>
<section class="subscribe-section">
	<div class="container">
		<div class="row row-fit">
			<div class="col-md-12 col-lg-9 col-lg-offset-2">
				<div class="subscribe-box <?php tt_print($css.' '.$el_class) ?>">
					<div class="icon">
						<i class="icon-mail-envelope"></i>
					</div>

					<div class="content">
						<h3 class="title"><?php tt_print($subs_title) ?></h3>
						<p class="description"><?php tt_print($subs_text) ?></p>
						<form class="subscribe-form" action="#" method="post" data-tt-subscription id="subscription_form">
							<div class="input-line">
								<input type="text" class="form-input check-value" data-tt-subscription-required data-tt-subscription-type="email" name="email">
								<span class="placeholder"><?php tt_print($subs_placeholder) ?><span><?php _ex('*', 'subscription-form', 'materialism') ?></span></span>
								<span class="line-border"></span>
							</div>
							<button class="submit"><?php tt_print($subs_button_text) ?></button>
							<div class="result_container"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>