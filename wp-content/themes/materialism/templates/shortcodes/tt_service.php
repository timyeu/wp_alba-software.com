<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
vc_icon_element_fonts_enqueue( $type );
$icon = ${"icon_" . $type}; 

if($service_type === 'type_1') : ?>
<div class="feature-item">
	<i class="f-icon <?php tt_print($icon); tt_print(' '.$css_class.' '.$el_class); ?>" <?php print $color_icon ? 'style="color: '.$color_icon.'"' : '';?>></i>
	<h3 <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php tt_print($title);?></h3>
	<p <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php tt_print($subtext); ?></p>
</div>
<?php elseif($service_type === 'type_2') : ?>
	<div class="service-item">
		<i class="s-icon <?php tt_print($icon); tt_print(' '.$css_class.' '.$el_class); ?>" <?php print $color_icon ? 'style="color: '.$color_icon.'"' : '';?>></i>
		<h3 class="title" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php tt_print($title);?></h3>
		<p class="description" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php tt_print($subtext); ?></p>
	</div>
<?php endif; ?>