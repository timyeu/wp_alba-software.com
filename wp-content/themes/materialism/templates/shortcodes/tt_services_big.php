<?php
$page_slug = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$link_text = $link_text ? $link_text : 'READ MORE';

$sg_post = new WP_Query( array( 'name' => $page_slug, 'post_type' => 'page' ) );


if($sg_post->have_posts()) : while($sg_post->have_posts()) : $sg_post->the_post(); ?>
		<div class="service-item-big">
			<a href="<?php the_permalink() ?>">
			<?php if(has_post_thumbnail()) : the_post_thumbnail('tt_small_post_thumbnail'); else : echo '<img src="' . get_template_directory_uri() . '/images/no-image-big.jpg" alt="'.get_the_title().'">'; endif; ?>
			</a>

			<div class="meta">
				<h3><?php the_title() ?></h3>
				<a class="check" href="<?php the_permalink() ?>"><?php tt_print($link_text) ?></a>
			</div>
		</div>
<?php endwhile; endif; ?>