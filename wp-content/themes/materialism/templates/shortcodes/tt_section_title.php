<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$output = '<div class="section-title '.$css_class.' '.$el_class.'">' ;
$output .= !empty($title) ? '<h2>'.$title.'</h2>' : '';
$output .= !empty($subtext) ? '<p>'.$subtext.'</p>' : '';
$output .= '</div>';

print balanceTags($output);
?>