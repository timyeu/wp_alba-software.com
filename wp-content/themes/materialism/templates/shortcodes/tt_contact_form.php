<?php
$css = $section_id = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$section_id = !empty($section_id) ? $section_id : 'contact';
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>

<section id="<?php tt_print($section_id); ?>" class="contact-section form section <?php tt_print($css); ?>">
	<span></span>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php tt_form_location('shortcode'); ?>
			</div>
		</div>
	</div>
</section>