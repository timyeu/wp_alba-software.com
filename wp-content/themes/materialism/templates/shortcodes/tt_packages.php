<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$features_items = (array) vc_param_group_parse_atts( $features_items );
$button_text = $button_text ? $button_text : 'Buy Now';
$item_url = $item_url ? $item_url : '#';
?>

<div class="pricing-table gradient">
	<h4 class="title"><?php tt_print($title) ?></h4>
	<span class="pricing"><?php tt_print($price) ?></span>
	<ul class="features clean-list">
		<?php foreach($features_items as $features_item): ?>
			<li><?php tt_print($features_item['item_feature']); ?></li>
		<?php endforeach; ?>
	</ul>

	<a class="btn buy-btn gradient" href="<?php tt_print($item_url) ?>"><?php tt_print($button_text) ?></a>
</div>
