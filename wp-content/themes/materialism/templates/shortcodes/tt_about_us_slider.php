<?php
$slide_items = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$slide_items = (array) vc_param_group_parse_atts( $slide_items );
$section_id = !empty($section_id) ? $section_id : 'about-us';
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>


<div class="slick-carousel gallery-carousel" data-speed="800" data-dots="true" data-arrows="false" data-infinite="true" data-items-desktop="1" data-items-mobile="1" data-items-xs-mobile="1">
	<div class="carousel-items">
		<?php foreach($slide_items as $slide_item): ?>
			<?php $slide_img = wp_get_attachment_image_src($slide_item['item_image'], 'original') ?>
			<div class="carousel-item">
				<img src="<?php tt_print($slide_img[0]); ?>" alt="gallery image">
				<?php if(!empty($slide_item['item_caption'])) : ?>
					<span class="caption"><?php tt_print($slide_item['item_caption']); ?></span>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>						
	</div>
</div>