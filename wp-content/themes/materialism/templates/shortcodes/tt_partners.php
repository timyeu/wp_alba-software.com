<?php
$partenr_items = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$partner_items = (array) vc_param_group_parse_atts( $partner_items );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>

<ul class="partners-list clean-list clearfix <?php tt_print($css.' '.$el_class); ?>">
	<?php foreach($partner_items as $partner_item): ?>
		<?php $partner_img = wp_get_attachment_image_src($partner_item['item_image'], 'original') ?>
		<li><a href="<?php tt_print($partner_item['item_url']); ?>"><img src="<?php tt_print($partner_img[0]); ?>" alt="partner"></a></li>
	<?php endforeach; ?>
</ul>