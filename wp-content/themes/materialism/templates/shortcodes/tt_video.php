<?php 
$embed_url = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$title = $title ? $title : '';

$placeholder = wp_get_attachment_image_src($placeholder, 'original');
?>

<section class="video-section <?php tt_print($el_class.' '.$css); ?>" <?php if($placeholder) : print 'style="background: url('.$placeholder[0].'); background-size: cover;"'; else : print 'style="background: #242430;"'; endif; ?>>
	<div class="container">
		<h3 class="title"><?php tt_print($title) ?></h3>
		<div class="video-toggle-s" data-embed="<?php tt_print( $embed_url ) ?>"></div>
	</div>
</section>