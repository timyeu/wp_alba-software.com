<?php
$about_us_hide_button = $about_us_button_link = $about_us_button_text = $about_us_content = $about_us_title = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$about_us_title = !empty($about_us_title) ? $about_us_title : 'About Us';
$about_us_button_text = !empty($about_us_button_text) ? $about_us_button_text : 'Learn more';
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>

<div class="about-wrapper <?php tt_print($css) ?>">
	<h2 class="title text-gradient"><?php tt_print($about_us_title); ?></h2>

	<div class="description">
		<?php print $about_us_content; ?>
	</div>
	<?php if(!$about_us_hide_button) : ?>
		<a class="btn big gradient" href="<?php tt_print($about_us_button_link); ?>"><?php tt_print($about_us_button_text); ?></a>
	<?php endif; ?>
</div>
