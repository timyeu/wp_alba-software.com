<?php
$grafic_number = $grafic_title = $color_grafic = $progress_items = $css = $section_id = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$progress_items = (array) vc_param_group_parse_atts( $progress_items );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>

<div class="graph-item clearfix <?php tt_print($css); ?>">
	<?php if($grafic_title || $grafic_number) : ?>
		<div class="stats-item">
			<?php if($grafic_title) : ?>
				<span class="caption"><?php tt_print($grafic_title); ?></span>
			<?php endif; ?>
			<?php if($grafic_number) : ?>
				<span class="number"><?php tt_print($grafic_number); ?></span>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="progress-group">
		<?php foreach($progress_items as $progress_item) : ?>
			<div class="progress-item vertical <?php tt_print($el_class); ?>">
				<div class="progress-bar" <?php print $color_grafic ? 'style="background: '.$color_grafic.'"' : '' ;?> data-number="<?php tt_print($progress_item['item_bar_percentage']); ?>"></div>
			</div>
		<?php endforeach; ?>
	</div>
</div>