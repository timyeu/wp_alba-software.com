<?php
/**
 * @var $atts
 */
$twitter_background = $tw_user = $tw_count = $tw_consumerkey = $tw_consumersecret = $tw_accesstoken = $tw_accesstokensecret = $background_style = $css = $bg_class = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$bg_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$twitter_background = wp_get_attachment_image_src($twitter_background, 'original') ? wp_get_attachment_image_src($twitter_background, 'original') : '';
$step_callback = '';
$tw_count = 1;
if(empty($tw_consumerkey) || empty($tw_consumersecret) || empty($tw_accesstoken) || empty($tw_accesstokensecret) ) {
    echo '<h5>' . _x('Please fill all Twitter fields', 'Twitter shortcode','materialism') . '</h5>';
    return;
}
$args = array(
    'consumerkey' => $tw_consumerkey,
    'consumersecret' => $tw_consumersecret,
    'accesstoken' => $tw_accesstoken,
    'accesstokensecret' => $tw_accesstokensecret,
); ?>

<section class="twitter-section <?php echo "$bg_class $el_class" ?>"<?php if($twitter_background) echo 'style="background: url('. $twitter_background[0] .'); background-size: cover;"'; ?>>
	<div class="container">
		<div class="twitter-tweet">
			<i class="icon-twitter icon"></i>
				<?php echo tt_twitter_generate_output($tw_user, $tw_count, $callback = '', $step_callback, $before = '<ul class="clean-list tweet-list">', $after = '</ul>', $args); ?>
		</div>
	</div>
</section>