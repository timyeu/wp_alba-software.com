<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$url_link = $url_link ? $url_link : '#';
$text = $text ? $text : 'Button';
?>

<a class="btn <?php tt_print($css.' '.$el_class) ?>" style="<?php if($text_color) echo "color: ".$text_color." !important;"; if($back_color) echo "background: ".$back_color." !important;"; ?>" href="<?php tt_print($url_link); ?>"><?php tt_print($text); ?></a>