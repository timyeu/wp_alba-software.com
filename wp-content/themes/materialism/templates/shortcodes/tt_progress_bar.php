<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

if($bar_style === 'slider') : ?>
	<div class="progress-wrapper">
		<div class="progress-item">
			<div class="progress-bar" data-number="<?php tt_print($percentage) ?>">
				<span class="heading">
					<span class="number"><?php tt_print($percentage) ?></span><?php esc_html_e('%', 'materialism') ?>
				</span>
				<span class="caption"><?php tt_print($tooltip_text) ?></span>
			</div>
		</div>
	</div>
<?php elseif($bar_style === 'indicator') : ?>
	<div class="progress-wrapper">
		<div class="progress-item v2">
			<div class="progress-bar" data-number="<?php tt_print($percentage) ?>">
				<span class="heading">
					<span class="number"><?php tt_print($percentage) ?></span>
				</span>
				<span class="caption"><?php tt_print($tooltip_text) ?></span>
			</div>
		</div>
	</div>
<?php endif; ?>