<div class="col-sm-6">
	<div <?php post_class('blog-post-item'); ?> id="post-<?php the_ID(); ?>">
		<img class="post-cover" src="<?php if(has_post_thumbnail()) the_post_thumbnail_url('tt_small_post_thumbnail'); else echo get_template_directory_uri() . '/images/no-image-big.jpg'; ?>" alt="<?php the_title() ?>">
		<div class="post-meta">
			<h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
			<?php the_category() ?>
			<a class="read-more" href="<?php the_permalink() ?>"><?php esc_html_e('read more', 'materialism') ?></a>
		</div>
	</div>
</div>