<?php
$page_id = tt_get_page_id();
$number_of_related = get_post_meta( $page_id, THEME_NAME . '_related_posts', true );
if($number_of_related === '0' || !$number_of_related) $number_of_related = '2';


$related_query = '';
$port_tax = array();

	$terms = wp_get_post_terms( $page_id, 'portfolio_tax');
	$cats = '';
	foreach($terms as $term)
		array_push($port_tax, $term->slug);

	$related_query = new WP_Query( array(
		'post_type' => 'portfolio',
		'post_status' => 'publish',
		'tax_query' => array(
			array(
				'taxonomy' => 'portfolio_tax',
				'field'    => 'slug',
				'terms'    => $port_tax,
			),
		),
		'post__not_in' => array($page_id),
		'posts_per_page'=> $number_of_related
	)); ?>

	<div class="related-posts">
		<div class="container">
			<div class="single-share-block">
	    		<span class="circle-btn share-btn"></span>
	    		<?php tt_share(); ?>
	    	</div>
		
			<h3 class="box-title"><?php esc_html_e('Other Portfolio Posts', 'materialism') ?></h3>
		</div>

		<div class="container-fluid max-container">
			<div class="row">
				<?php if(!empty($related_query) && $related_query->have_posts()) : while($related_query -> have_posts()) : $related_query->the_post();
				$options = get_post_meta(get_the_ID(), 'slide_options', true); 
				$post_thumbnail_url = wp_get_attachment_url( $options['full_image'] ); ?>
					<div class="col-sm-6">
						<div class="service-item-big">
							<a href="<?php the_permalink() ?>">
								<img src="<?php tt_print($post_thumbnail_url) ?>" alt="<?php get_the_title( ) ?>">
							</a>

							<div class="meta">
								<h3><?php the_title() ?></h3>
								<a class="check" href="<?php the_permalink() ?>"><?php esc_html_e('Check out full project >', 'materialism') ?></a>
							</div>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
