<?php
$page_id = tt_get_page_id();
$number_of_related = get_post_meta( $page_id, THEME_NAME . '_related_posts', true );
if($number_of_related === '0' || !$number_of_related) $number_of_related = '2';

$orig_post = $post;
global $post;
$categories = get_the_category($post->ID);

if ($categories) :
    $category_ids = array();
        foreach( $categories as $individual_category ) $category_ids[] = $individual_category->term_id;
        $args = array(
                        'category__in' => $category_ids,
                        'post__not_in' => array($post->ID),
                        'posts_per_page'=> $number_of_related // Number of related posts that will be shown.
        );

    $my_query = new wp_query( $args );

    if( $my_query->have_posts() ) : ?>
        <div class="related-posts">
            <div class="container">
                <div class="single-share-block">
                    <span class="circle-btn share-btn"></span>
                    <?php tt_share(); ?>
                </div>
                <h3 class="box-title"><?php esc_html_e('Related Posts','materialism'); ?></h3>
            </div>

            <div class="container-fluid">
                <div class="row row-fit">
                <?php while( $my_query->have_posts() ) : $my_query->the_post(); ?>
                    <div class="col-sm-6">
                        <div class="blog-post-item">
                            <img class="post-cover" src="<?php if(has_post_thumbnail()) the_post_thumbnail_url('tt_small_post_thumbnail'); else echo get_template_directory_uri() . '/images/no-image-small.jpg'; ?>" alt="<?php the_title(); ?>">
                            <div class="post-meta">
                                <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <?php the_category(); ?>
                                <a class="read-more" href="<?php the_permalink(); ?>"><?php esc_html_e('read more','materialism') ?></a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
<?php endif; 
endif;
$post = $orig_post;