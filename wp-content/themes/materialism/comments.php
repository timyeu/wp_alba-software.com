<?php
wp_reset_postdata();
if(comments_open( ) || have_comments()) : ?>
    <?php if ( post_password_required() ) : ?>
        <p><?php esc_attr_e( 'This post is password protected. Enter the password to view any comments ', 'materialism' ); ?></p>

        <?php
        /* Stop the rest of comments.php from being processed,
         * but don't kill the script entirely -- we still have
         * to fully load the template.
         */
        return;
        endif;
        $args = array(
            'fields' => apply_filters( 'comment_form_default_fields', array(

                'author'    => '<div class="input-line">
                                    <input type="text" class="form-input check-value" name="author" value="' . esc_attr( $commenter[ 'comment_author' ] ) . '" aria-required="true">
                                    <span class="placeholder">'.esc_attr_x('Your Name','comment-form','materialism').'<span>'.esc_attr_x('*','comment-form','materialism').'</span></span>
                                    <span class="line-border"></span>
                                </div>',

                'email'     =>  '<div class="input-line">
                                    <input type="text" class="form-input check-value" name="email" value="' . esc_attr( $commenter[ 'comment_author_email' ] ) . '" aria-required="true">
                                    <span class="placeholder">'.esc_attr_x('E-mail address','comment-form','materialism').'<span>'.esc_attr_x('*','comment-form','materialism').'</span></span>
                                    <span class="line-border"></span>
                                </div>',


                'url'       =>  '<div class="input-line">
                                    <input type="text" class="form-input check-value" name="url" value="' . esc_attr( $commenter[ 'comment_author_url' ] ) . '" aria-required="false">
                                    <span class="placeholder">'.esc_attr_x('Website URL','comment-form','materialism').'</span>
                                    <span class="line-border"></span>
                                </div>'

                )
            ),
            'comment_notes_after' => '',
            'comment_notes_before' => '',
            'class_form'    =>  'respond-form',
            'title_reply_before' => '',
            'title_reply_after' => '',
            'title_reply'   => '',
            'comment_field' => '<div class="input-line">
                                    <textarea name="comment" class="form-input check-value"></textarea>
                                    <span class="placeholder">'.esc_attr_x('Comment','comment-form','materialism').'<span>'.esc_attr_x('*','comment-form','materialism').'</span></span>
                                    <span class="line-border textarea"></span>
                                </div>',
            'label_submit'  => esc_attr_x('POST COMMENT','comment-form','materialism'),
            'class_submit'  =>  'btn form-submit purple'
        );
    if ( have_comments() && comments_open()) :
        paginate_comments_links(); ?>
        <!-- Respond Area -->
            <div class="comments-area">
                <ul class="clean-list comments-list">
                    <?php wp_list_comments( array( 'callback' => 'tt_custom_comments' , 'avatar_size'=>'70','style'=>'ul') ); ?>
                </ul>
            </div>
    <?php endif; ?>
    <div class="reply-area">
        <?php comment_form( $args ); ?>
    </div>
<?php endif;