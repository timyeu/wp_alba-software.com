		</div>
		<footer>
			<div class="container">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'secondary',
							'container' => false,
							'menu_class' => 'footer-menu clean-list'
						)
					);
				?>
				<ul class="social-links clean-list">
					<?php $social_platforms = array('facebook','twitter','google','pinterest','linkedin','dribbble','behance','youtube','flickr','rss','instagram');
					foreach($social_platforms as $platform):
						if (_go('social_platforms_' . $platform)): ?>
							<li><a href="<?php echo _go('social_platforms_' . $platform) ?>" target="_blank"><i class="fa fa-<?php echo esc_attr( $platform ) ?>"></i></a></li>
						<?php endif;
					endforeach ?>
				</ul>
				<?php if(!_go('hide_copyright')) : ?>
					<div class="col-md-12">
					    <p class="copyright"><?php if(_go('copyright_text')) : _eo('copyright_text'); else : esc_html_e('Designed by ','materialism');?><a href="<?php echo esc_url('http://teslathemes.com','materialism')?>"><?php esc_html_e('Teslathemes', 'materialism'); ?></a>, <?php esc_html_e('Supported by ', 'materialism'); ?><a href="<?php echo esc_url('https://wpmatic.io'); ?>"><?php esc_html_e('WPMatic', 'materialism'); ?></a><?php endif; ?></p>
					</div>
				<?php endif; ?>
			</div>
		</footer>
	</div>
	<?php wp_footer(); ?>
</body>
</html>