<?php get_header(); ?>
	<section class="hero-section simple" <?php if(_go('default_hero_image')) : echo 'style="background: url('._go('default_hero_image').'); background-size: cover;"'; else : echo 'style="background: #242433;"'; endif; ?>>
		<div class="container-fluid">
			<h1 class="hero-title"><?php esc_html_e('Category: ', 'materialism'); single_cat_title(); ?></h1>
		</div>
	</section>

	<section class="blog-section">
		<div class="container-fluid">
			<div class="row row-fit">

				<?php 
					if (have_posts()) :
					$i=1; 
						while (have_posts()) : the_post();
							if($i % 3 == 0) : 
								get_template_part( 'templates/content-full');
							else : 
								get_template_part( 'templates/content-half');
							endif;
							$i++;
						endwhile;
						get_template_part('templates/pagination');
					endif;
				?>`
			</div>
		</div>
	</section>
<?php get_footer(); ?>