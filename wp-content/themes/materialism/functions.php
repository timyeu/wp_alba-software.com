<?php

/***********************************************************************************************/
/*  Tesla Framework */
/***********************************************************************************************/

require_once(get_template_directory() . '/tesla_framework/tesla.php');

/***********************************************************************************************/
/*  Register Plugins */
/***********************************************************************************************/

if ( is_admin() && current_user_can( 'install_themes' ) ) {
  require_once( get_template_directory() . '/plugins/tgm-plugin-activation/register-plugins.php' );
}

/***********************************************************************************************/
/* Add Menus */
/***********************************************************************************************/

function tt_register_menus($return = false){
	$tt_menus = array('primary' => esc_attr_x('Main Menu', 'dashboard','materialism'), 
						'secondary' => esc_attr_x('Footer Menu', 'dashboard', 'materialism'));
	if($return)
		return array('primary' => 'Main Menu', 'secondary' => 'Footer Menu');
	register_nav_menus($tt_menus);
}
add_action('init', 'tt_register_menus');

/**********************************************************************************************/
/*    favicon                                                                                 */
/**********************************************************************************************/
function tt_theme_favicon() {
	if( function_exists( 'wp_site_icon' ) && has_site_icon() ) {
		wp_site_icon();
	} else if(_go( 'favicon')){
		echo "\r\n" . sprintf( '<link rel="shortcut icon" href="%s">', _go( 'favicon') ) . "\r\n";
	}
}
add_action( 'wp_head', 'tt_theme_favicon');

/***********************************************************************************************/
/*   Enable Visual Composer */
/***********************************************************************************************/

function tt_load_vc() {
	if (class_exists('Vc_Manager')) {
		vc_set_shortcodes_templates_dir(TEMPLATEPATH . '/templates/shortcodes');
		require_once(TEMPLATEPATH . '/theme_config/shortcodes.php');
		require_once(TEMPLATEPATH . '/theme_config/tt-map.php');
	}
}
add_action('init','tt_load_vc');

/***********************************************************************************************/
/* ESC Attribute Function */
/***********************************************************************************************/

function tt_print( $param ){
	print esc_attr( $param );
}

/**********************************************************************************************/
/*        SIDEBARS                                                                            */
/**********************************************************************************************/

function tt_materialism_main_sidebar_register() {

  register_sidebar( array(
	'name'          => 'Main Sidebar',
	'id'            => 'main-sidebar',
	'before_widget' => '<div class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h4 class="widget-title">',
	'after_title'   => '</h4>',
  ) );
}
add_action( 'widgets_init', 'tt_materialism_main_sidebar_register' );


/***********************************************************************************************/
/* Share Function */
/***********************************************************************************************/

if(!function_exists('tt_share')){
  function tt_share(){
	$share_this = _go('share_this');
	if(isset($share_this) && is_array($share_this)): ?>

			<!-- Share Article -->
				<ul class="clean-list social-platforms">
					<li class="title"><?php esc_html_e('Share', 'materialism') ?></li>
				  <?php foreach($share_this as $val):
					if($val === 'googleplus') $val = 'google-plus';
					switch ($val) {
					  case 'facebook': ?>
							  <?php printf('<li class="platform %s">', $val) ?>
							<a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;t=<?php urlencode(get_the_title()); ?>&amp;u=<?php echo urlencode(get_the_permalink()) ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i><?php esc_html_e(' Facebook','materialism') ?></a>
						
						<?php break; ?>
					  <?php case 'twitter': ?>
						  <?php printf('<li class="platform %s">', $val) ?>
							<a class="twitter" onClick="window.open('http://twitter.com/intent/tweet?url=<?php echo urlencode(get_the_permalink()) ?>&amp;text=<?php urlencode(get_the_title()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i><?php esc_html_e(' Twitter','materialism') ?></a>
						  
						<?php break; ?>
					  <?php case 'google-plus': ?>
						  <?php printf('<li class="platform %s">', $val) ?>
							<a class="google-plus" onClick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()) ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i><?php esc_html_e(' Google+','materialism') ?></a>
						 
						<?php break; ?>
					  <?php case 'pinterest': ?>
						  <?php printf('<li class="platform %s">', $val) ?>
							<a class="pinterest" onClick="window.open('https://www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_the_permalink()) ?>','sharer','toolbar=0,status=0,width=748,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i><?php esc_html_e(' Pinterest','materialism') ?></a>
						  
						<?php break; ?>
					  <?php case 'linkedin': ?>
						  <?php printf('<li class="platform %s">', $val) ?>
							<a class="linkedin" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(get_the_permalink()) ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i><?php esc_html_e(' Linkedin','materialism') ?></a>
						  
						  <?php break; ?>
					  <?php case 'vkontakte': ?>
						  <?php printf('<li class="platform %s">', $val) ?>
							<a class="vkontakte" onClick="window.open('http://www.vkontakte.ru/share.php?url=<?php echo urlencode(get_the_permalink()) ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-vk"></i><?php esc_html_e(' Vkontakte','materialism') ?></a>
						  
						<?php break; ?>
					  <?php default:
						esc_attr_e('No Share','materialism');
						break;
					} ?>
				  <?php endforeach; ?>
			  </ul>
		<!-- Share Article End -->
	<?php endif;
	}
}

/***********************************************************************************************/
/* Comments */
/***********************************************************************************************/

function tt_custom_comments( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);
	global $comment_order;
	$comment_order++;
	$comment_class = 'comment-li';
	if($comment_order == 1)
		$comment_class = 'first';
  if ( 'div' == $args['style'] ) {
	$tag = 'div';
	$add_below = 'comment';
  } else {
	$tag = 'li';
	$add_below = 'div-comment';
  }
  ?>

  <<?php print $tag ?> id="comment-<?php comment_ID() ?>" <?php comment_class(empty( $args['has_children'] ) ? " $comment_class " : " $comment_class parent ") ?>>
			
	<div class="comment-body">
		<?php if ($args['avatar_size'] != 0): ?>
			<div class="avatar">
				<?php echo get_avatar( $comment, $args['avatar_size'], false,'avatar image' ); ?>
			</div>
		<?php endif ?>

		<h4 class="user-title"><?php echo get_comment_author(); ?></h4>
		<span class="comment-date"><?php echo get_comment_time(get_option('date_format','j F, Y \a\t g:i a')) ?></span>
	
		<div class="message"><?php comment_text() ?>
			<?php if ($comment->comment_approved == '0') : ?>
				<em class="comment-awaiting-moderation">
					<i class="fa-fa-gear fa-spin"></i><?php esc_attr_e(' Your comment is awaiting moderation.','materialism') ?>
				</em>
				<br />
			<?php endif;?>
		</div>
		<?php 
			edit_comment_link(__('Edit','materialism'),'<i class="fa fa-pencil"></i> ','' ); 
			comment_reply_link(array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=> esc_html__('Reply','materialism')))); 
		?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>"></div>
	<?php endif;
}

function move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields;
}

add_filter( 'comment_form_fields', 'move_comment_field_to_bottom' );

/***********************************************************************************************/
/* Color Changers */
/***********************************************************************************************/

add_action( 'tt_fw_init' , 'tt_color_changers' );
function tt_color_changers(){
	//color changers
	require_once get_template_directory() . '/theme_config/color_changers.php';
}

/***********************************************************************************************/
/* Custom JS */
/***********************************************************************************************/

add_action('wp_footer', 'tt_custom_js', 99);
function tt_custom_js() {
  ?>
  <script type="text/javascript"><?php echo esc_js(_eo('custom_js')) ?></script>
  <?php
}

/***********************************************************************************************/
/* DISPLAYING POST POPULARITY BY VIEWS ( VIEWS COUNTER )                                       */
/***********************************************************************************************/

function tt_getPostViews($postID){
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
	}
	return $count.' Views';
}
function tt_setPostViews($postID) {
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}

/***********************************************************************************************/
/* Form Location                                                                               */
/***********************************************************************************************/
add_action( 'tt_fw_init' , 'tt_fw_functions' );
function tt_fw_functions(){
  TT_Contact_Form_Builder::add_form_locations(array(
	  'shortcode' => 'Shortcode Form',
  ));

/***********************************************************************************************/
/* Google fonts + Fonts changer */
/***********************************************************************************************/

TT_ENQUEUE::$gfont_changer = array(
		_go('logo_text_font'),
		_go('main_content_text_font'),
		_go('sidebar_text_font'),
		_go('menu_text_font')
	);
TT_ENQUEUE::$base_gfonts = array('Roboto:400,300,500,700,100');
TT_ENQUEUE::add_css(array('//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'));
}

/***********************************************************************************************/
/* AJAX Functions */
/***********************************************************************************************/
add_action('wp_ajax_post_likes', 'materialism_portfolio_likes');
add_action('wp_ajax_nopriv_post_likes', 'materialism_portfolio_likes');

function materialism_portfolio_likes(){
	if(!empty($_POST['postid'])){
		$post_id = $_POST['postid'];
		$likes = get_post_meta($post_id, 'tt_materialism_post_likes', true);
		
		if( $_COOKIE['tt_materialism_post_likes_'. $post_id] !== null ) :
			if($likes) $likes--;
			if(update_post_meta($post_id, 'tt_materialism_post_likes', $likes)){
				unset($_COOKIE['tt_materialism_post_likes_'. $post_id]);
				setcookie('tt_materialism_post_likes_'. $post_id, '', time()*20, '/');
				die(true);
			}
			else{
				die('error');
			}
		else :
			if(!$likes) $likes = 0;
			$likes++;
			
			if(update_post_meta($post_id, 'tt_materialism_post_likes', $likes)){
				setcookie('tt_materialism_post_likes_'. $post_id, 'tt_'.$post_id, time()*20, '/');
				die(true);
			}
			else{
				die('error');
			}
		endif;
	}
die();
}


function post_type_tags_fix($request) {
	if ( isset($request['tag']) && !isset($request['post_type']) )
	$request['post_type'] = 'any';
	return $request;
} 
add_filter('request', 'post_type_tags_fix');