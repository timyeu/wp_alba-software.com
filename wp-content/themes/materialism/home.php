<?php get_header(); 
/**
 * Blog Page
 */
$page_id = tt_get_page_id();
$hide_header = get_post_meta($page_id, THEME_NAME . '_hide_header', true);
$single_post_shortcodes = get_post_meta( $page_id, THEME_NAME . '_single_post_shortcodes', true );


?>
	<section class="hero-section simple" style="<?php if($page_id) : if(has_post_thumbnail($page_id)) : echo "background: url('". get_the_post_thumbnail_url( $page_id ,'tt_big_post_thumbnail') ."') center center; background-size: cover;"; endif; elseif(_go('default_hero_image')) :
		echo "background: url("._go('default_hero_image')."); background-size: cover;";	else : echo "background: #242433;"; endif; ?>">
		<div class="container-fluid">
			<h1 class="hero-title"><?php if($page_id) echo get_the_title($page_id); else esc_html_e('The News | Your daily noise of inspirations', 'materialism'); ?></h1>
		</div>
	</section>

	<section class="blog-section">
		<div class="container-fluid">
			<div class="row row-fit">

				<?php 
					if (have_posts()) :
					$i=1; 
						while (have_posts()) : the_post();
							if($i % 3 == 0) : 
								get_template_part( 'templates/content-full');
							else : 
								get_template_part( 'templates/content-half');
							endif;
							$i++;
						endwhile;
						get_template_part('templates/pagination');
					endif;
				?>
			</div>
		</div>
	</section>
<?php 

if($single_post_shortcodes) :
	$shortcodes_list = str_replace(', ', ',', $single_post_shortcodes); 
	$shortcodes = explode(",", $shortcodes_list); 
	foreach ($shortcodes as $shortcod) :
		print do_shortcode( $shortcod );
	endforeach;
endif;

get_footer(); ?>