<?php get_header(); ?>
	<section class="hero-section simple" <?php if(_go('default_hero_image')) : echo 'style="background: url('._go('default_hero_image').'); background-size: cover;"'; else : echo 'style="background: #242433;"'; endif; ?>>
		<div class="container-fluid">
			<h1 class="hero-title"><?php esc_html_e('Search: ', 'materialism'); echo get_search_query(); ?></h1>
		</div>
	</section>

	<section class="blog-section">
		<div class="container-fluid">
			<div class="row row-fit">

				<?php 
					if (have_posts()) :
					$i=1; 
						while (have_posts()) : the_post();
							if($i % 3 == 0) : 
								get_template_part( 'templates/content-full');
							else : 
								get_template_part( 'templates/content-half');
							endif;
							$i++;
						endwhile;
						get_template_part('templates/pagination');
					else : ?>
						<div class="section-title search">
							<h2><?php esc_html_e('There are no posts that would match your search query.', 'materialism'); ?></h2>
							<h3><?php esc_html_e('Please try again.', 'materialism'); ?></h3>
						</div>
					<?php endif;?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>