<form class="search-form" method="get" id="search-form" action="<?php echo esc_url( home_url('/') ) ?>">
	<input type="text" class="form-input" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="<?php esc_attr_e('Search','materialism') ?>" >
	<button class="form-submit">
		<i class="icon-search"></i>
	</button>
</form>