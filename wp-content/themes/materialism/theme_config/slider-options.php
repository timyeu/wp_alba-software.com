<?php
return array('team' => array(
		'name' => 'Team',
		'term' => 'team',
		'term_plural' => 'Members',
		'order' => 'DESC',
		'has_single' => false,
		'hierarchical' => false,
		'label'               => 'Team',
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'post_options' => array(
			'supports' => array( 'title'),
			'taxonomies' => array(),
			'has_archive'=> false,
			'exclude_from_search' => true
		),
		'taxonomy_options' => array('show_ui' => true),
		'options' => array(
			'team_member' => array(
				'type' => array(
					'full_name' => array(
						'type' => 'line',
						'title' => 'Full Name',
						'description' => 'example: Brian Neeson'
					),
					'member_avatar' => array(
						'type' => 'image',
						'description' => 'Click on the placeholder to select an image.',
						'title' => 'Photo',
						'default' => 'holder.js/150x150/auto',
					),
					'speciality' => array(
						'type' => 'line',
						'title' => 'Speciality',
						'description' => 'example: Web Developer',
					),
					'email' => array(
						'type' => 'line',
						'title' => 'E-mail',
						'description' => 'example: brian@teslathemes.com'
					),
					'description' => array(
						'type' => 'text',
						'title' => 'Some Info',
						'description' => 'example: "Smart, beautiful and helpful".'
					),

				),
				'title' => 'Member Info',
				'multiple' => false,
				'group' => true
			),
			'user_meta' => array(
				'type' => array(
					'meta_text' => array(
						'type' => 'line',
						'title' => 'Stats Title',
						'description' => 'example "Projects"',
					),
					'meta_number' => array(
						'type' => 'line',
						'title' => 'Quantity',
						'description' => 'Use a number, for example Number of projects: "20".'
					)
				),
				'title' => 'Number of Projects',
				'description' => 'You can add up to 3 pieces of meta, let\'s say Projects, Coffees, Awards.',
				'multiple' => true,
				'group' => true
			),
			
			'social_profiles' => array(
				'type' => array(
					'social_logo' => array(
						'type' => 'line',
						'title' => 'Social Icon',
						'description' => 'Write a social network\'s name, it accepts words like: <br><b>facebook, facebook-sqare, twitter, google-plus, pinterest, instagram, tumblr, vk.</b> for more icons visit fontawesome.io',
					),
					'social_link' => array(
						'type' => 'line',
						'title' => 'Social profile link',
						'description' => 'example: https://facebook.com/teslathemes'
					)
				),
				'title' => 'Social profiles',
				'description' => 'You can add up to 3 social profiles.',
				'multiple' => true,
				'group' => true
			)
		),
		'icon' => 'icons/16x16.png',
		'output' => array(

			'main' => array(
				'shortcode' => 'tt_team',
				'view' => 'views/team-shortcode',
				'shortcode_defaults' => array(
					'center_item_background' => '',
					'desktop_items' => '',
					'tablet_items' => '',
					'mobile_items' => '',
					'infinite_slick' => '',
					'section_id' => '',
					'el_class' => '',
					'css' => ''
				)
			),
			'single' => array(
				'view'                  => 'views/team-view',
				'shortcode_defaults'    => array(
				)
			)
		)
	),
	'testimonials' => array(
		'name' => 'Testimonials',
		'term' => 'testimonials',
		'term_plural' => 'Testimonials',
		'order' => 'DESC',
		'has_single' => false,
		'hierarchical' => false,
		'label'               => 'Testimonials',
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'post_options' => array(
			'supports' => array( 'title'),
			'taxonomies' => array(),
			'has_archive'=> false,
			'exclude_from_search' => true
		),
		'taxonomy_options' => array('show_ui' => true),
		'options' => array(
			'testimonials' => array(
				'type' => array(
					'testimonial_name' => array(
						'type' => 'line',
						'title' => 'Full Name',
						'description' => 'example: Brian Neeson'
					),
					'testimonial_avatar' => array(
						'type' => 'image',
						'description' => 'Click on the placeholder to select an image.',
						'title' => 'Photo',
						'default' => 'holder.js/160x160/auto',
					),
					'testimonial_url' => array(
						'type' => 'line',
						'title' => 'Url',
						'description' => 'example: www.teslathemes.com'
					),
					'testimonial_feedback' => array(
						'type' => 'text',
						'title' => 'A few words...',
						'description' => 'example: "I like Teslathemes because ...".'
					),

				),
				'title' => 'Testimonial Info',
				'multiple' => false,
				'group' => true
			)
		),
		'icon' => 'icons/16x16.png',
		'output' => array(

			'main' => array(
				'shortcode' => 'tt_testimonials',
				'view' => 'views/testimonials-shortcode',
				'shortcode_defaults' => array(
					'testimonial_background' => '',
					'section_id' => '',
					'el_class' => '',
					'css' => ''
				)
			),
			'single' => array(
				'view'                  => 'views/testimonials-view',
				'shortcode_defaults'    => array(
				)
			)
		)
	),

	'portfolio' => array(
		'name' => 'Portfolio',
		'term' => 'portfolio',
		'term_plural' => 'portfolios',
		'order' => 'DESC',
		'has_single' => true,
		'heirarchical' => true,
		'show_in_menu'        => true,
       		'show_in_nav_menus'   => true,
       		'label' => 'Portfolios',
		'url' => _go('portfolio_url') ? _go('portfolio_url') : 'portfolio',
		'post_options' => array(
			'supports'=> array( 'title','thumbnail'),
			'taxonomies' => array('post_tag'),
			'has_archive'=>true,
			'exclude_from_search' => false
			),
		'taxonomy_options' => array('show_ui' => true),
		'options' => array(
			'full_image' => array(
				'type' => 'image',
				'description' => 'Portfolio item header Image',
				'title' => 'Full Size',
				'default' => 'holder.js/1920x400/auto'
			),
			'video'	=>	array(
				'title' => 'Video Link',
				'description' => 'Insert an embed link for a youtube video.',
				'type' => 'line',
				'default' => ''
			),
			'description'	=>	array(
				'title' => 'Description',
				'description' => 'Insert project description.',
				'type' => 'text',
				'default' => ''
			),

			'quote'	=>	array(
				'type' => array(
					'quote_text' => array(
						'type' => 'text',
						'title' => 'The Quote'
					),
					'quote_author' => array(
						'type' => 'line',
						'title' => 'Quote\'s Author',
						'description' => 'For example: "Bill Gates"'
					)
				),
			),
		),
		'icon' => 'icons/16x16.png',
		'output' => array(
			'main' => array(
				'shortcode' => 'tt_portfolio',
				'view' => 'views/portfolio-shortcode',
				'shortcode_defaults' => array(
					
					'style' => '',
					'el_class' => '',
					'css' => ''
				)
			),
		),
	),   
);