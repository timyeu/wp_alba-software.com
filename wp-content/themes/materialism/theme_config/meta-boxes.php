<?php
return array(
		'metaboxes'=>array(
			array(
				'id'             => 'post_settings_',          // meta box id, unique per meta box
				'title'          => esc_attr_x('Post Settings','meta boxes','materialism'),         // meta box title,'meta boxes','materialism')e
				'post_type'      => array('post'),
				'priority'		 => 'low',
				'context'		=> 'normal',
				'input_fields'   => array(            // list of meta fields (can be added by field arrays)
					'sidebar_position'  =>  array(
						'name'  =>  esc_attr_x('Sidebar Position','meta boxes','materialism'),
						'type'  =>  'select',
						'values'    =>  array(
								'as_blog'   =>  esc_attr_x('Same as Blog Page','meta boxes','materialism'),
								'full_width'=>  esc_attr_x('No Sidebar/Full Width','meta boxes','materialism'),
								'right'     =>  esc_attr_x('Right','meta boxes','materialism'),
								'left'      =>  esc_attr_x('Left','meta boxes','materialism'),
							),
						'std'   =>  'as_blog'  //default value selected
					),
					'related_posts_on_off'  =>  array(
						'name'  =>  esc_attr_x('Related Posts visibility','meta boxes','materialism'),
						'type'  =>  'select',
						'values'    =>  array(
								'show_related_posts'	=>  esc_attr_x('Show Related Posts','meta boxes','materialism'),
								'hide_related_posts'	=>  esc_attr_x('Hide Related Posts','meta boxes','materialism')
							),
						'std'   =>  'show_related_posts',  //default value selected
						'desc'	=> esc_attr_x('Choose to show or hide the "related posts" section.','meta boxes', 'materialism')
					),
					'related_posts'  =>  array(
						'name'  =>  esc_attr_x('Number of Related Posts','meta boxes','materialism'),
						'type'  =>  'number',
						'desc'	=> esc_attr_x('Number of related posts under the comments field.','meta boxes', 'materialism')
					),
					'single_post_shortcodes'  =>  array(
						'name'  =>  esc_attr_x('Single Post Shortcodes','meta boxes','materialism'),
						'type'  =>  'textarea',
						'desc'	=> esc_attr_x('Insert a shortcodes in a format like: " [tt_subscribe] ". For multiple Shortcodes use commas to separate them like this: " [tt_subscribe], [tt_subscribe] "','meta boxes', 'materialism')

					)
				)
			),
			
			array(
				'id'             => 'page_settings_',            // meta box id, unique per meta box
				'title'          => esc_attr_x('Page Settings','meta boxes','materialism'),   // meta box title
				'post_type'      => array('page'),		// post types, accept custom post types as well, default is array('post'); optional
				'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
				'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
				'priority'		 => 'low',						// order of meta box: high (default), low; optional
				'input_fields'   => array(
					
					
					'sidebar_position'  =>  array(
						'name'  =>  esc_attr_x('Sidebar Position','meta boxes','materialism'),
						'type'  =>  'select',
						'values'    =>  array(
								'full_width'    =>  esc_attr_x('No Sidebar/Full Width','meta boxes','materialism'),
								'right'         =>  esc_attr_x('Right','meta boxes','materialism'),
								'left'          =>  esc_attr_x('Left','meta boxes','materialism'),
							),
						'std'   =>  'full_width'  //default value selected
					),
					'single_post_shortcodes'  =>  array(
						'name'  =>  esc_attr_x('Single Post Shortcodes','meta boxes','materialism'),
						'type'  =>  'textarea',
						'desc'	=> esc_attr_x('Insert a shortcodes in a format like: " [tt_subscribe] ". For multiple Shortcodes use commas to separate them like this: " [tt_subscribe], [tt_subscribe] "','meta boxes', 'materialism')

					)
				)
			),
			array(
				'id'             => 'header_settings_',            // meta box id, unique per meta box
				'title'          => esc_attr_x('Header Settings','meta boxes','materialism'),   // meta box title
				'post_type'      => array('page'),		// post types, accept custom post types as well, default is array('post'); optional
				'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
				'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
				'priority'		 => 'high',						// order of meta box: high (default), low; optional
				'input_fields'   => array(
					
					'hide_header'  =>  array(
						'name'  =>  esc_attr_x('Header Style','meta boxes','materialism'),
						'type'  =>  'select',
						'values'    =>  array(
								'normal_header'    =>  esc_attr_x('Normal Hero Header','meta boxes','materialism'),
								'no_header'          =>  esc_attr_x('No Header','meta boxes','materialism'),
							),
						'std'   =>  'normal_header',
						'desc'	=> 'You can hide the hero header and use something else instead, something like a Shortcode.'
					),
				)
			),
			array(
				'id'             => 'porfolio_settings_',          // meta box id, unique per meta box
				'title'          => esc_attr_x('Portfolio Settings','meta boxes','materialism'),         // meta box title,'meta boxes','materialism')e
				'post_type'      => array('portfolio'),
				'priority'		 => 'low',
				'context'		=> 'normal',
				'input_fields'   => array(            // list of meta fields (can be added by field arrays)
					'related_posts_on_off'  =>  array(
						'name'  =>  esc_attr_x('Related Posts visibility','meta boxes','materialism'),
						'type'  =>  'select',
						'values'    =>  array(
								'show_related_posts'	=>  esc_attr_x('Show Related Portfolios','meta boxes','materialism'),
								'hide_related_posts'	=>  esc_attr_x('Hide Related Portfolios','meta boxes','materialism')
							),
						'std'   =>  'show_related_posts',  //default value selected
						'desc'	=> esc_attr_x('Choose to show or hide the "related posts" section.','meta boxes', 'materialism')
					),
					'related_posts'  =>  array(
						'name'  =>  esc_attr_x('Number of Related Portfolios','meta boxes','materialism'),
						'type'  =>  'number',
						'desc'	=> esc_attr_x('Number of related portfolios.','meta boxes', 'materialism')
					),
					'single_post_shortcodes'  =>  array(
						'name'  =>  esc_attr_x('Single Portfolio Shortcodes','meta boxes','materialism'),
						'type'  =>  'textarea',
						'desc'	=> esc_attr_x('Insert a shortcodes in a format like: " [tt_subscribe] ". For multiple Shortcodes use commas to separate them like this: " [tt_subscribe], [tt_subscribe] "','meta boxes', 'materialism')

					)
				)
			),
		)
	);