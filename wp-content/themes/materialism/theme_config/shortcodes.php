<?php
/* Contact Form */
class WPBakeryShortCode_TT_contact_form extends WPBakeryShortCode {
}
/* About Us Slider */
class WPBakeryShortCode_TT_about_us_slider extends WPBakeryShortCode {
}
/* About Us Section*/
class WPBakeryShortCode_TT_about_section extends WPBakeryShortCode {
}
/* Service */
class WPBakeryShortCode_TT_service extends WPBakeryShortCode {
}
/* Section Title */
class WPBakeryShortCode_TT_section_title extends WPBakeryShortCode {
}
/* Progress Grafic */
class WPBakeryShortCode_TT_progress_graf extends WPBakeryShortCode {
}
/* Single Post */
class WPBakeryShortCode_TT_single_post extends WPBakeryShortCode {
}
/* Subscription Form */
class WPBakeryShortCode_TT_subscribe extends WPBakeryShortCode {
}
/* Partners */
class WPBakeryShortCode_TT_partners extends WPBakeryShortCode {
}
/* Pricing Tables */
class WPBakeryShortCode_TT_packages extends WPBakeryShortCode {
}
/* Hero Section */
class WPBakeryShortCode_TT_hero_section extends WPBakeryShortCode {
}
/* Video */
class WPBakeryShortCode_TT_video extends WPBakeryShortCode {
}
/* Progress Bar */
class WPBakeryShortCode_TT_progress_bar extends WPBakeryShortCode {
}
/* Button */
class WPBakeryShortCode_TT_button extends WPBakeryShortCode {
}
/* Twitter */
class WPBakeryShortCode_TT_twitter extends WPBakeryShortCode {
}
/* Services Big */
class WPBakeryShortCode_TT_services_big extends WPBakeryShortCode {
}