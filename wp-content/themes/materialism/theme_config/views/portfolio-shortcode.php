<?php 
global $post;
$slide = $slides[0];
$post = $slide['post'];
setup_postdata($post);
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';
$style = $shortcode['style'] ? $shortcode['style'] : 'style_1';

$post_categories = get_terms( 'portfolio_tax', array( 'hide_empty' => true ) );
$post_cats['All Projects'] = 'all-projects';

if(!is_wp_error($post_categories)) :
    foreach($post_categories as $category) 
        $post_cats[$category->name] = $category->slug;
endif;

?>
<?php if($style === 'style_1') : ?>
<div class="tabbed-content grad <?php tt_print($css).' '.tt_print($el_class) ?>">
	<ul class="tabs">
		<?php foreach($post_cats as $category_name => $category_slug) : ?>
		<li class="tab-link <?php if($category_slug === 'all-projects') echo 'current'; ?>" data-tab="<?php tt_print($category_slug) ?>"><?php tt_print($category_name) ?></li>
	<?php endforeach;  ?>
	</ul>
	<ul class="tabbed-body clean-list more-padding">
		<li id="all-projects" class="tab-content current">
			<div class="row">
				<?php $i = 0;  foreach ( $slides as $slide_nr => $slide ) :
					$post_cats = wp_get_post_terms( $slide['post']->ID,  'portfolio_tax' ); 	
					$i++; 		
					if( $i == 1) : ?>
						<div class="col-md-6">
							<div class="service-item-big">
								<a href="<?php echo get_the_permalink($slide['post']->ID);?>">
									<?php echo wp_get_attachment_image(get_post_thumbnail_id($slide['post']->ID), 'tt_portfolio_big' ); ?>
								</a>

								<div class="meta">
									<h3><?php echo get_the_title($slide['post']->ID) ?></h3>
									<a class="check" href="<?php echo get_the_permalink($slide['post']->ID);?>"><?php esc_html_e('Check out all the features &gt;','materialism') ?></a>
								</div>
							</div>
						</div>
					<?php continue;  endif; ?>
						<div class="col-md-3">
							<div class="service-item-big small">
								<a href="<?php echo get_the_permalink($slide['post']->ID);?>"><?php echo wp_get_attachment_image(get_post_thumbnail_id($slide['post']->ID), 'tt_portfolio_small'); ?></a>
								<?php if($slide['options']['video']) : ?>
									<div class="video-toggle" data-embed='<?php tt_print($slide['options']['video']) ?>'></div>
								<?php endif; ?>
							</div>
						</div>
				<?php endforeach ?>
			</div>
		</li>
		<?php foreach ($post_categories as $catteg) :  ?>
			<li id="<?php print esc_attr($catteg->slug); ?>" class="tab-content">
				<div class="row">
					<?php foreach ($slides as $slide_nr => $slide) :  
						$post_cats = wp_get_post_terms( $slide['post']->ID,  'portfolio_tax' ); 
						foreach($post_cats as $slug) :
							if($slug->slug == $catteg->slug ) : ?> 
								<div class="col-md-3">
									<div class="service-item-big small">
										<a href="<?php echo get_the_permalink($slide['post']->ID);?>"><?php echo wp_get_attachment_image(get_post_thumbnail_id($slide['post']->ID), 'tt_portfolio_small'); ?></a>
										<?php if($slide['options']['video']) : ?>
											<div class="video-toggle" data-embed='<?php tt_print($slide['options']['video']) ?>'></div>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; 
						endforeach; 
					endforeach; ?>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
<?php elseif($style === 'style_2') : ?>
<div class="tabbed-content">
	<ul class="tabs">
		<?php foreach($post_cats as $category_name => $category_slug) : ?>
		<li class="tab-link <?php if($category_slug === 'all-projects') echo 'current'; ?>" data-tab="<?php tt_print($category_slug) ?>"><?php tt_print($category_name) ?></li>
	<?php endforeach;  ?>
	</ul>

    	<ul class="tabbed-body clean-list">

		<li id="all-projects" class="tab-content current">
			<div class="row">
				<?php  foreach ( $slides as $slide_nr => $slide ) : ?>
					<div class="col-md-2 col-sm-4">
						<a href="<?php echo get_the_permalink($slide['post']->ID) ?>"><?php echo wp_get_attachment_image(get_post_thumbnail_id($slide['post']->ID), 'tt_portfolio_small'); ?></a>
					</div>
				<?php endforeach; ?>
			</div>
		</li>
		<?php foreach ($post_categories as $catteg) :  ?>
			<li id="<?php print esc_attr($catteg->slug); ?>" class="tab-content">
				<div class="row">
					<?php foreach ($slides as $slide_nr => $slide) :  
					$post_cats = wp_get_post_terms( $slide['post']->ID,  'portfolio_tax' ); 
					if($post_cats[0]->slug == $catteg->slug ) : ?>
						<div class="col-md-2 col-sm-4">
							<a href="<?php echo get_the_permalink($slide['post']->ID) ?>"><?php echo wp_get_attachment_image(get_post_thumbnail_id($slide['post']->ID), 'tt_portfolio_mob'); ?></a>
						</div>
					<?php endif; endforeach; ?>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>