<?php
global $post;
$slide = $slides[0];
$post = $slide['post'];
setup_postdata($post);
$testimonial_background = $shortcode['testimonial_background'] ? $shortcode['testimonial_background'] : '';
$background = wp_get_attachment_image_src($testimonial_background, 'original');
$section_id = $shortcode['section_id'] ? $shortcode['section_id'] : 'testimonials';
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css_testimonials']) ? vc_shortcode_custom_css_class( $shortcode['css_testimonials'] ) : '';
?>
<section id="<?php tt_print($section_id); ?>" class="testimonials-section section <?php tt_print($css_class); ?>" style="<?php if($testimonial_background !== '') echo "background: url('".$background[0]."'); background-size: cover;"; ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="slick-carousel testimonials-carousel" data-speed="800" data-dots="true" data-infinite="true" data-items-desktop="1" data-items-mobile="1" data-items-xs-mobile="1">
					<div class="carousel-items">
						<?php foreach($slides as $slide) : ?>
							<div class="carousel-item">
								<div class="testimonial">
									<p class="message"><?php tt_print($slide['options']['testimonials']['testimonial_feedback']); ?></p>

									<h3 class="name"><?php tt_print($slide['options']['testimonials']['testimonial_name']); ?></h3>
									<span class="position"><?php tt_print($slide['options']['testimonials']['testimonial_url']); ?></span>

									<div class="cover aligncenter">
										<img class="avatar" src="<?php tt_print($slide['options']['testimonials']['testimonial_avatar']); ?>" alt="testimonial member">
									</div>
								</div>
							</div>
						<?php endforeach; ?>
						<?php wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>