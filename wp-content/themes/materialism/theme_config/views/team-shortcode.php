<?php
global $post;
$slide = $slides[0];
$post = $slide['post'];
setup_postdata($post);
$center_item_background = $shortcode['center_item_background'] ? $shortcode['center_item_background'] : '';
$background = wp_get_attachment_image_src($center_item_background, 'original');
$desktop_items = $shortcode['desktop_items'] ? $shortcode['desktop_items'] : '3';
$tablet_items = $shortcode['tablet_items'] ? $shortcode['tablet_items'] : '3';
$mobile_items = $shortcode['mobile_items'] ? $shortcode['mobile_items'] : '1';
$infinite_slick = $shortcode['infinite_slick'] ? $shortcode['infinite_slick'] : 'true';
$section_id = $shortcode['section_id'] ? $shortcode['section_id'] : 'team';
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';

?>

<section id="<?php tt_print($section_id); ?>" class="team-section section <?php tt_print($css_class); ?>">
	<div class="container">
		<div class="slick-carousel team-members" data-center-mode="true" data-speed="800" data-dots="false" data-arrows="true" data-infinite="<?php tt_print($infinite_slick) ?>" data-items-desktop="<?php tt_print($desktop_items) ?>" data-items-tablet="<?php tt_print($tablet_items) ?>" data-items-mobile="<?php tt_print($mobile_items) ?>" data-items-xs-mobile="1">
			<div class="carousel-items row">

			<?php foreach($slides as $slide) : ?>
			
				<div class="carousel-item col-md-4">
					<div class="team-member">
						<img class="cover" src="<?php tt_print($slide['options']['team_member']['member_avatar']); ?>" alt="team-member">
						<h3 class="name text-gradient"><?php tt_print($slide['options']['team_member']['full_name']); ?></h3>
						<span class="position"><?php tt_print($slide['options']['team_member']['speciality']); ?></span>
						<p><?php tt_print($slide['options']['team_member']['description']); ?></p>
					</div>
					<div class="team-member big">
						<div class="top-box" style="<?php if($center_item_background !== '') echo "background: url('".$background[0]."');"; ?>">
							<img class="cover" src="<?php tt_print($slide['options']['team_member']['member_avatar']); ?>" alt="team-member">
							<h3 class="name text-gradient"><?php tt_print($slide['options']['team_member']['full_name']); ?></h3>
							<span class="position"><?php tt_print($slide['options']['team_member']['speciality']); ?></span>

							<ul class="stats-list clean-list clearfix">
								<?php foreach( $slide['options']['user_meta'] as $user_meta ) : ?>
									<li>
										<span class="number"><?php tt_print($user_meta['meta_number']); ?></span>
										<span class="text"><?php tt_print($user_meta['meta_text']); ?></span>
									</li>
								<?php endforeach; ?>
								<a class="circle-btn contact-btn" href="mailto:<?php tt_print($slide['options']['team_member']['email']); ?>"></a>
							</ul>
						</div>
						<div class="bottom-box">
							<p class="description"><?php tt_print($slide['options']['team_member']['description']); ?></p>
							<ul class="socials clean-list">
								<?php foreach( $slide['options']['social_profiles'] as $social_profile ) : ?>
									<li><a href="<?php tt_print($social_profile['social_link']); ?>"><i class="fa fa-<?php tt_print($social_profile['social_logo']); ?>"></i></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>

			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>