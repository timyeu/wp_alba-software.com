<?php
$vc_is_wp_version_3_6_more = version_compare( preg_replace( '/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo( 'version' ) ), '3.6' ) >= 0;

add_filter( 'vc_iconpicker-type-icomoon', 'tt_icon');

$colors_arr = array (
	esc_html__( 'Blue', 'materialism' ) => 'blue',
	esc_html__( 'Turquoise', 'materialism' ) => 'turquoise',
	esc_html__( 'Pink', 'materialism' ) => 'pink',
	esc_html__( 'Violet', 'materialism' ) => 'violet',
	esc_html__( 'Peacoc', 'materialism' ) => 'peacoc',
	esc_html__( 'Chino', 'materialism' ) => 'chino',
	esc_html__( 'Mulled Wine', 'materialism' ) => 'mulled_wine',
	esc_html__( 'Vista Blue', 'materialism' ) => 'vista_blue',
	esc_html__( 'Black', 'materialism' ) => 'black',
	esc_html__( 'Orange', 'materialism' ) => 'orange',
	esc_html__( 'Sky', 'materialism' ) => 'sky',
	esc_html__( 'Green', 'materialism' ) => 'green',
	esc_html__( 'Juicy pink', 'materialism' ) => 'juicy_pink',
	esc_html__( 'Sandy brown', 'materialism' ) => 'sandy_brown',
	esc_html__( 'Purple', 'materialism' ) => 'purple',
	esc_html__( 'White', 'materialism' ) => 'white',
);


function tt_icon($icons) {
	$icomoon = array(
		array('icon-photo-camera2' => 'Camera'),
		array('icon-speedometer' => 'Speedometer'),
		array('icon-imac' => 'IMac'),
		array('icon-headphones' => 'Headphones'),
		array('icon-stack-of-three-coins' => 'Stack Coins'),
		array('icon-light-bulb' => 'Light Bulb'),
		array('icon-view' => 'View'),
		array('icon-settings' => 'Gear'),
		array('icon-user' => 'User'),
		array('icon-microphone' => 'Microphone'),
		array('icon-notepad' => 'Notepad'),
		array('icon-glasses' => 'Glasses'),
		array('icon-coffee-cup' => 'Coffee'),
		array('icon-star' => 'Star'),
		array('icon-fountain-pen' => 'Pen'),
	);
	return array_merge($icons, $icomoon);
}

function tt_icons() {
	$icons = array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'materialism' ),
				'value' => array(
					esc_html__( 'Font Awesome', 'materialism' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'materialism' ) => 'openiconic',
					esc_html__( 'Typicons', 'materialism' ) => 'typicons',
					esc_html__( 'Entypo', 'materialism' ) => 'entypo',
					esc_html__( 'Linecons', 'materialism' ) => 'linecons'

				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select icon library.', 'materialism' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'materialism' ),
				'value' => array(
					esc_html__( 'Font Awesome', 'materialism' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'materialism' ) => 'openiconic',
					esc_html__( 'Typicons', 'materialism' ) => 'typicons',
					esc_html__( 'Entypo', 'materialism' ) => 'entypo',
					esc_html__( 'Linecons', 'materialism' ) => 'linecons'
				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select icon library.', 'materialism' ),
				'dependency' => array(
					'element' => 'add_icon',
					'value' => 'true',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'materialism' ),
				'param_name' => 'icon_fontawesome',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'materialism' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'materialism' ),
				'param_name' => 'icon_openiconic',
				'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'openiconic',
				),
				'description' => esc_html__( 'Select icon from library.', 'materialism' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'materialism' ),
				'param_name' => 'icon_typicons',
				'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'typicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'materialism' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'materialism' ),
				'param_name' => 'icon_entypo',
				'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'entypo',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'materialism' ),
				'param_name' => 'icon_linecons',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'linecons',
				),
				'description' => esc_html__( 'Select icon from library.', 'materialism' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'materialism' ),
				'param_name' => 'icon_icomoon',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'icomoon',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'icomoon',
				),
				'description' => esc_html__( 'Select icon from library.', 'materialism' ),
			));
	return $icons;
}
$tt_icons = tt_icons();

//  Add Parameters to row
vc_add_params('vc_row', array(
	array(
		'type' => 'checkbox',
		'heading' => esc_attr__( 'Show Title?', 'materialism' ),
		'param_name' => 'sc_title',
		'description' => esc_attr__( 'If checked Title will be shown at top', 'materialism' ),
		'value' => array( esc_attr__( 'Yes', 'materialism' ) => 'yes' )
	),
	array(
		'type' => 'textfield',
		'heading' => esc_attr__( 'Section title', 'materialism' ),
		'param_name' => 'section_title',
		'value' => '',
		'description' => esc_attr__( 'Set Section title', 'materialism' ),
		'dependency' => array(
			'element' => 'sc_title',
			'not_empty' => true,
		),
	),
	array(
		'type' => 'dropdown',
		'heading' => esc_attr__( 'Choose section style', 'materialism' ),
		'param_name' => 'sc_title_style',
		'description' => esc_attr__( 'Select style type.', 'materialism' ),
		'value'         =>  array(
			esc_attr__('Light', 'materialism')    =>  'standard',
			esc_attr__('Dark', 'materialism')     =>  'inverted'
		),
		'std'   =>  'standard'
	),
));

/* TT Team
----------------------------------------------------------- */

vc_map( array(
	'name' => esc_attr__( 'Team', 'materialism' ),
	'base' => 'tt_team',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Tesla Team', 'materialism' ),
	"params" => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_attr__( 'Background Image', 'materialism' ),
			'param_name' => 'center_item_background',
			'description' => esc_attr__( 'Choose the background image for the centered picture', 'materialism' ),
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Nr. of items on desktop', 'materialism' ),
			'param_name' => 'desktop_items',
			'description' => esc_attr__( 'Default: 3', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Nr. of items on tablet', 'materialism' ),
			'param_name' => 'tablet_items',
			'description' => esc_attr__( 'Default: 3', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Nr. of items on mobile devices', 'materialism' ),
			'param_name' => 'mobile_items',
			'description' => esc_attr__( 'Default: 1', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_attr__( 'Stop infinite scrolling ?', 'materialism' ),
			'param_name' => 'infinite_slick',
			'value' => array( esc_attr__( 'Yes, please.', 'materialism' ) => 'false' ),
			'description' => esc_attr__( 'Make the slider infinite.', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Section ID', 'materialism' ),
			'param_name' => 'section_id',
			'description' => esc_attr__( 'Default: team', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
) );

/* TT Testimonials
----------------------------------------------------------- */

vc_map( array(
	'name' => esc_attr__( 'Testimonials', 'materialism' ),
	'base' => 'tt_testimonials',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Tesla Testimonials', 'materialism' ),
	"params" => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_attr__( 'Background Image', 'materialism' ),
			'param_name' => 'testimonial_background',
			'description' => esc_attr__( 'Choose the background image for the testimonials section.', 'materialism' ),
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Section ID', 'materialism' ),
			'param_name' => 'section_id',
			'description' => esc_attr__( 'Default: testimonials', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css_testimonials',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
) );
/* Contact Form 
-----------------------------------------------------------*/
vc_map( array(
	'name' => esc_attr__( 'Tesla Contact Form', 'materialism' ),
	'base' => 'tt_contact_form',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'By using this shortcode you have to go to theme options and create a contact form.', 'materialism' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Section ID', 'materialism' ),
			'param_name' => 'section_id',
			'description' => esc_attr__( 'Default: contact', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
		)
	));

/* TT About Us Slider
----------------------------------------------------------- */
vc_map( array(
	'name'      => esc_attr__( 'About Us Slider', 'materialism' ),
	'base'      => 'tt_about_us_slider',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Create an about us section with slides.', 'materialism' ),
	'params' => array(
		array(
			'type' => 'param_group',
			'heading' => esc_attr__( 'Slide Item', 'materialism' ),
			'param_name' => 'slide_items',
			'description' => esc_attr__( 'Enter content for each slide (an image + a phrase if you want.)', 'materialism' ),
			'value' => urlencode( json_encode( array(
				array(
					'item_image'        =>  '',
					'item_caption'      =>  '',
				)))),
			'params'    =>  array(
				array(
					'type'  =>  'attach_image',
					'heading'   =>  esc_attr__('Slide Image','materialism'),
					'param_name'    =>  'item_image',
					'description'   =>  esc_attr__('Choose an image as a Slide.','materialism'),
					'admin_label' => true,
				),
				array(
					'type'  =>  'textfield',
					'heading'   =>  esc_attr__('Caption','materialism'),
					'param_name'    =>  'item_caption',
					'description'   =>  esc_attr__('Type item caption','materialism'),
				),
			)
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
));


/* TT About Us Text
----------------------------------------------------------- */
vc_map( array(
	'name'      => esc_attr__( 'About Us Section', 'materialism' ),
	'base'      => 'tt_about_section',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Create an about us section with slides.', 'materialism' ),
	'params' => array(
		
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Section Title', 'materialism' ),
			'param_name' => 'about_us_title',
			'description' => esc_attr__( 'Give this section a nice title.', 'materialism' )
		),
		array(
			'type' => 'textarea',
			'heading' => esc_attr__( 'Section Content', 'materialism' ),
			'param_name' => 'about_us_content',
			'description' => esc_attr__( 'Write some content.', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Button Text', 'materialism' ),
			'param_name' => 'about_us_button_text',
			'description' => esc_attr__( 'Text within the button.', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Button Link', 'materialism' ),
			'param_name' => 'about_us_button_link',
			'description' => esc_attr__( 'Choose where the button takes you.', 'materialism' )
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_attr__( 'Hide Section button', 'materialism' ),
			'param_name' => 'about_us_hide_button',
			'value' => array( esc_attr__( 'Yes, please.', 'materialism' ) => 'yes' ),
			'description' => esc_attr__( 'Check to hide the button.', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
));


/* TT Service */
vc_map( array(
	'name' => esc_html__( 'Service Box', 'materialism' ),
	'base' => 'tt_service',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'materialism' ), 'description' => esc_html__( 'Tesla Service Box', 'materialism' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Type', 'materialism' ),
			'value' => array(
				esc_html__( 'Type 1', 'materialism' ) => 'type_1',
				esc_html__( 'Type 2', 'materialism' ) => 'type_2',
			),
			'param_name' => 'service_type',
			'description' => esc_html__( 'Select service style type.', 'materialism' ),
			'std' => 'type_1',
			'admin_label' => true,
		),

		$tt_icons[0],
		$tt_icons[2],
		$tt_icons[3],
		$tt_icons[4],
		$tt_icons[5],
		$tt_icons[6],
		$tt_icons[7],

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Color', 'materialism' ),
			'param_name' => 'color_icon',
			'description' => esc_html__( 'Select icon color.', 'materialism' ),
		),        
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Title', 'materialism' ),
			'param_name' => 'title',
			'admin_label' => true,
			'value' => esc_html__( 'This is a service title', 'materialism' ),
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Description', 'materialism' ),
			'param_name' => 'subtext',
			'admin_label' => false,
			'value' => esc_html__( 'This is a small service description', 'materialism' ),
			'description' => esc_html__( 'If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'materialism' ),
			'description' => esc_html__( 'Note: If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'materialism' ),
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Text Color', 'materialism' ),
			'param_name' => 'text_color',
			'description' => esc_html__( 'Select text color for title & description.', 'materialism' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'materialism' )
		)
	)
) );

/* Section Title
-------------------------------------------------------------*/
vc_map( array(
	'name' => esc_html__( 'Section Title', 'materialism' ),
	'base' => 'tt_section_title',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'materialism' ),
	'description' => esc_html__( 'Tesla Section Title', 'materialism' ),
	'params' => array(
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Section Title', 'materialism' ),
			'param_name' => 'title',
			'admin_label' => true,
			'value' => esc_html__( 'This is custom heading title', 'materialism' ),
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Section Description', 'materialism' ),
			'param_name' => 'subtext',
			'admin_label' => true,
			'value' => esc_html__( 'This is a custom heading description', 'materialism' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a existing class name (black, white, hero-section) or another custom class from CSS.', 'materialism' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'materialism' )
		)
	),
) );

/* progresss graphic
--------------------------------------------------------*/
vc_map( array(
	'name'      => esc_attr__( 'Progress Graphic', 'materialism' ),
	'base'      => 'tt_progress_graf',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Create a progress grafic.', 'materialism' ),
	'params' => array(
		array(
			'type' => 'param_group',
			'heading' => esc_attr__( 'Vertical Progress Bar', 'materialism' ),
			'param_name' => 'progress_items',
			'description' => esc_attr__( 'NOTE: You can create up to 7 Lines (with the texts). If you create more than 7, the elements won\'t fit the container', 'materialism' ),
			'value' => urlencode( json_encode( array(
				array(
					'item_bar_percentage'        =>  ''
				)))),
			'params'    =>  array(
				array(
					'type'  =>  'textfield',
					'heading'   =>  esc_attr__('Percentage','materialism'),
					'param_name'    =>  'item_bar_percentage',
					'description'   =>  esc_attr__('Write a number from 1-100.','materialism'),
				)
			)
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Grafic Title', 'materialism' ),
			'param_name' => 'grafic_title',
			'description' => esc_attr__( 'Ex: "Projects".', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Grafic Number', 'materialism' ),
			'param_name' => 'grafic_number',
			'description' => esc_attr__( 'Ex: "1890".', 'materialism' )
		),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Grafic Lines Color', 'materialism' ),
			'param_name' => 'color_grafic',
			'description' => esc_html__( 'Select grafic color.', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
));

/* Blog Post
-------------------------------------------------------------*/
vc_map( array(
	'name' => esc_attr__( 'Single Post', 'materialism' ),
	'base' => 'tt_single_post',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Displays a single post by slug.', 'materialism' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_attr__( 'Style', 'materialism' ),
			'param_name' => 'post_style',
			'description' => esc_attr__( 'Select the style for this post.', 'materialism' ),
			'value'         =>  array(
				esc_attr__('Style 1 - Small', 'materialism')    =>  'style_1',
				esc_attr__('Style 2 - Small', 'materialism')     =>  'style_2',
				esc_attr__('Style 3 - Big', 'materialism')     =>  'style_3',
			),
			'std'   =>  'style_2'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Post Slug', 'materialism' ),
			'param_name' => 'post_slug',
			'description' => esc_attr__( 'Choose a post by its slug', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));
/* Subscription Form
----------------------------------------------*/
vc_map( array(
	'name' => esc_attr__( 'Subscription Form', 'materialism' ),
	'base' => 'tt_subscribe',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Displays the subscription form.', 'materialism' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Title', 'materialism' ),
			'param_name' => 'subs_title'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Description', 'materialism' ),
			'param_name' => 'subs_text'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Button Text', 'materialism' ),
			'param_name' => 'subs_button_text'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Input Placeholder', 'materialism' ),
			'param_name' => 'subs_placeholder'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));

/* Partners 
-----------------------------------------------------------------*/

vc_map( array(
	'name' => esc_attr__( 'Partners', 'materialism' ),
	'base' => 'tt_partners',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Displays a list of partners logos.', 'materialism' ),
	"params" => array(
		array(
			'type' => 'param_group',
			'heading' => esc_attr__( 'Partner Item', 'materialism' ),
			'param_name' => 'partner_items',
			'description' => esc_attr__( 'Enter content for each partner (an image + an url).', 'materialism' ),
			'value' => urlencode( json_encode( array(
				array(
					'item_image'        =>  '',
					'item_url'      =>  '',
				)))),
			'params'    =>  array(
				array(
					'type'  =>  'attach_image',
					'heading'   =>  esc_attr__('Partner Logo','materialism'),
					'param_name'    =>  'item_image',
					'description'   =>  esc_attr__('Choose an image as a partner logo.','materialism'),
					'admin_label' => true,
				),
				array(
					'type'  =>  'textfield',
					'heading'   =>  esc_attr__('URL Link','materialism'),
					'param_name'    =>  'item_url',
					'description'   =>  esc_attr__('Paste an url link.','materialism'),
				),
			)
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));


/* Pricing tables
-----------------------------------------------------------------*/

vc_map( array(
	'name' => esc_attr__( 'Pricing Tables', 'materialism' ),
	'base' => 'tt_packages',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Displays a list of pricing tables.', 'materialism' ),
	"params" => array(
		// array(
		// 	'type' => 'attach_image',
		// 	'heading' => esc_attr__( 'Background Image', 'materialism' ),
		// 	'param_name' => 'background_image',
		// 	'description' => esc_attr__( 'Choose a background.', 'materialism' ),
		// 	'admin_label' => true
		// ),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Title', 'materialism' ),
			'param_name' => 'title',
			'description' => esc_attr__( 'Give this box a title.', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Price', 'materialism' ),
			'param_name' => 'price',
			'description' => esc_attr__( 'Give this box a price.', 'materialism' )
		),
		array(
					'type'  =>  'textfield',
					'heading'   =>  esc_attr__('Button Text','materialism'),
					'param_name'    =>  'button_text',
					'description'   =>  esc_attr__('Text like: "Buy Now"','materialism'),
		),
		array(
					'type'  =>  'textfield',
					'heading'   =>  esc_attr__('Button URL Link','materialism'),
					'param_name'    =>  'item_url',
					'description'   =>  esc_attr__('Paste an url link.','materialism'),
		),
		array(
			'type' => 'param_group',
			'heading' => esc_attr__( 'Features', 'materialism' ),
			'param_name' => 'features_items',
			'description' => esc_attr__( 'Enter a list of features.', 'materialism' ),
			'value' => urlencode( json_encode( array(
				array(
					'item_feature'        =>  ''
				)))),
			'params'    =>  array(
				array(
					'type'  =>  'textfield',
					'heading'   =>  esc_attr__('Feature','materialism'),
					'param_name'    =>  'item_feature'
				),
			)
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));

/* Hero Section
-----------------------------------------------------------*/

vc_map( array(
	'name' => esc_attr__( 'Hero Section', 'materialism' ),
	'base' => 'tt_hero_section',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Hero Section Creator', 'materialism' ),
	"params" => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_attr__( 'Hero Background', 'materialism' ),
			'param_name' => 'hero_background',
			'description' => esc_attr__( 'Choose a background.', 'materialism' ),
			'admin_label' => true
		),
		array(
			'type' => 'attach_image',
			'heading' => esc_attr__( 'Brand Logo', 'materialism' ),
			'param_name' => 'hero_brand',
			'description' => esc_attr__( 'Choose your brand logo.', 'materialism' ),
			'admin_label' => true
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_attr__( 'Hide Brand Logo ?', 'materialism' ),
			'param_name' => 'hide_hero_brand',
			'value' => array( esc_attr__( 'Yes, please.', 'materialism' ) => 'yes' ),
			'description' => esc_attr__( 'Check to hide the Brand Logo.', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Hero Title', 'materialism' ),
			'param_name' => 'hero_title',
			'description' => esc_attr__( 'Insert here the title.', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Hero Sub-title', 'materialism' ),
			'param_name' => 'hero_subtitle',
			'description' => esc_attr__( 'Insert here the sub-title.', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Hero Button Text', 'materialism' ),
			'param_name' => 'hero_button_text',
			'description' => esc_attr__( 'Insert here the text for the hero button.', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Hero Button Link', 'materialism' ),
			'param_name' => 'hero_button_link',
			'description' => esc_attr__( 'Paste an url link here.', 'materialism' ),
			'admin_label' => true,
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_attr__( 'Hide the button ?', 'materialism' ),
			'param_name' => 'hide_hero_button',
			'value' => array( esc_attr__( 'Yes, please.', 'materialism' ) => 'yes' ),
			'description' => esc_attr__( 'Check to hide the Button.', 'materialism' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_attr__( 'Hide the scroll button ?', 'materialism' ),
			'param_name' => 'hide_hero_scroll_button',
			'value' => array( esc_attr__( 'Yes, please.', 'materialism' ) => 'yes' ),
			'description' => esc_attr__( 'Check to hide the Scroll Button.', 'materialism' ),
		),
		 array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'hero_css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));

/* Video Section
----------------------------------------------*/
vc_map( array(
	'name' => esc_attr__( 'Video Section', 'materialism' ),
	'base' => 'tt_video',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Video embed (iframe embed).', 'materialism' ),
	"params" => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_attr__( 'Placeholder Image', 'materialism' ),
			'param_name' => 'placeholder'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Section Title', 'materialism' ),
			'param_name' => 'title'
		),
		array(
			'type' => 'textarea',
			'heading' => esc_attr__( 'Embed Code', 'materialism' ),
			'param_name' => 'embed_url'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
	)
));
/* Progress Bar
----------------------------------------------------------------*/

vc_map( array(
	'name' => esc_attr__( 'Progress Bar', 'materialism' ),
	'base' => 'tt_progress_bar',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_attr__( 'Choose bar style', 'materialism' ),
			'param_name' => 'bar_style',
			'description' => esc_attr__( 'Select style type.', 'materialism' ),
			'value'         =>  array(
				esc_attr__('Slider', 'materialism')    =>  'slider',
				esc_attr__('Indicator', 'materialism')     =>  'indicator'
			),
			'std'   =>  'slider'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__('Progress Percentage', 'materialism'),
			'param_name' =>  'percentage',
			'description' => 'Enter a number from 1 to 100',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__('Tooltip Text', 'materialism'),
			'param_name' =>  'tooltip_text',
			'description' => 'Enter a number from 1 to 100',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
));

/* Button
-----------------------------------------------------------------------------*/

vc_map( array(
	'name' => esc_attr__( 'Button', 'materialism' ),
	'base' => 'tt_button',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	"params" => array(
		 array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Background Color', 'materialism' ),
			'param_name' => 'back_color',
			'description' => esc_html__( 'Select button background color.', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__('Text', 'materialism'),
			'param_name' =>  'text',
			'description' => 'Enter a number from 1 to 100',
		),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Text Color', 'materialism' ),
			'param_name' => 'text_color',
			'description' => esc_html__( 'Select button text color.', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__('URL Link', 'materialism'),
			'param_name' =>  'url_link',
			'description' => 'Enter a number from 1 to 100',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)
	)
));



/* TT Twitter */
vc_map( array(
	'name'      => __( 'Twitter', 'materialism' ),
	'base'      => 'tt_twitter',
	'category' => __( 'TeslaThemes', 'materialism' ),
	'description' => __( 'Create a section with tweets', 'materialism' ),
	'params' => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_attr__( 'Twitter section background', 'materialism' ),
			'param_name' => 'twitter_background'
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Username', 'materialism' ),
			'param_name' => 'tw_user',
			'description' => __( 'Tweeter username', 'materialism' ),
		),
		//array(
			//'type' => 'textfield',
			//'heading' => __( 'Nr. of tweets', 'materialism' ),
			//'param_name' => 'tw_count',
			//'description' => __( 'Maximum number of tweets to be loaded', 'materialism' ),
		//),
		array(
			'type' => 'textfield',
			'heading' => __( 'Consumer key', 'materialism' ),
			'param_name' => 'tw_consumerkey',
			'description' => __( 'Tweeter consumer key', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Consumer secret', 'materialism' ),
			'param_name' => 'tw_consumersecret',
			'description' => __( 'Tweeter consumer secret', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Access token', 'materialism' ),
			'param_name' => 'tw_accesstoken',
			'description' => __( 'Tweeter access token', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Access token secret', 'materialism' ),
			'param_name' => 'tw_accesstokensecret',
			'description' => __( 'Tweeter access token secret', 'materialism' ),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => __( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => __( 'Design Options', 'materialism' )
		)
	)
));

/* Page services (Changelog)
-------------------------------------------------------------*/
vc_map( array(
	'name' => esc_attr__( 'Services Big', 'materialism' ),
	'base' => 'tt_services_big',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'Displays a single service page by slug.', 'materialism' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Page Slug', 'materialism' ),
			'param_name' => 'page_slug',
			'description' => esc_attr__( 'Choose a page by its slug', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Link text', 'materialism' ),
			'param_name' => 'link_text',
			'description' => esc_attr__( 'Something like "Click to view" or "Read More".', 'materialism' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));

/* Tabbed Content
-------------------------------------------------------------*/
vc_map( array(
	'name' => esc_attr__( 'Portfolio Tabs', 'materialism' ),
	'base' => 'tt_portfolio',
	'icon' => 'icon-wpb',
	'category' => esc_attr__( 'TeslaThemes', 'materialism' ),
	'description' => esc_attr__( 'The Portfolio Tabs.', 'materialism' ),
	"params" => array(
		
		array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Tabs Style', 'materialism' ),
				'value' => array(
					esc_html__( 'Big Images', 'materialism' ) => 'style_1',
					esc_html__( 'Small Images', 'materialism' ) => 'style_2',
				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select a tab style.', 'materialism' ),
			),
		
		array(
			'type' => 'textfield',
			'heading' => esc_attr__( 'Extra class name', 'materialism' ),
			'param_name' => 'el_class',
			'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'materialism' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_attr__( 'CSS box', 'materialism' ),
			'param_name' => 'css',
			'group' => esc_attr__( 'Design Options', 'materialism' )
		)        
		)
	));