<?php
define('THEME_NAME', 'materialism');
define('THEME_PRETTY_NAME', 'Materialism');

//Load Textdomain
function tt_theme_textdomain_setup(){
    if(load_theme_textdomain('materialism', file_exists(get_stylesheet_directory() . '/languages') ? get_stylesheet_directory() . '/languages' : get_template_directory() . '/languages'))
        define('TT_TEXTDOMAIN_LOADED',true);
}
add_action('after_setup_theme', 'tt_theme_textdomain_setup');



//content width
if (!isset($content_width))
    $content_width = 1170;

//============Theme support=======
//post-thumbnails
add_theme_support('post-thumbnails');
//add feed support
add_theme_support('automatic-feed-links');
//add  html5 support
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'caption' ));
add_theme_support( 'custom-header' );
add_theme_support( 'custom-background' );
 add_theme_support( 'title-tag' );

add_image_size( 'tt_big_post_thumbnail', 1660, 620, array( 'center', 'center'));
add_image_size( 'tt_small_post_thumbnail', 830, 622, array( 'center', 'center'));
add_image_size( 'tt_popular_posts', 300, 100, array( 'center', 'center'));
add_image_size( 'tt_portfolio_small', 355, 280, array( 'center', 'center'));
add_image_size( 'tt_portfolio_big', 740, 588, array( 'center', 'center'));
add_image_size( 'tt_portfolio_mob', 277, 496, array( 'center', 'center'));
add_image_size( 'tt_portfolio_single', 780, 620, array( 'center', 'center'));