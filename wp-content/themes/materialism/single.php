<?php
get_header();
$page_id = tt_get_page_id();
$news_id = get_option( 'page_for_posts' );
$sidebar_option = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$sidebar_status = is_active_sidebar('main-sidebar' );
$related_posts_on_off = get_post_meta( $page_id, THEME_NAME . '_related_posts_on_off', true );
$single_post_shortcodes = get_post_meta( $page_id, THEME_NAME . '_single_post_shortcodes', true );

switch ($sidebar_option) {
	case 'as_blog':
		$s_id = $news_id;   
		break;
	case 'full_width':
		$s_id = $page_id;
		break;
	case 'right':
		$s_id = $page_id;
		break;
	case 'left':
		$s_id = $page_id;
}
if(!empty($s_id)) $sidebar = get_post_meta( $s_id, THEME_NAME . '_sidebar_position', true );
if(empty($sidebar)) $sidebar = 'full_width';
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); tt_setPostViews($post->ID); ?>
	<section class="hero-section single" style="<?php if(has_post_thumbnail()) echo "background: url('".get_the_post_thumbnail_url( $post, 'tt_big_post_thumbnail')."') center center;"; else echo "background: #242433;"; ?>">
		<div class="container">
			<div class="centered-content">
				<h1 class="post-title"><?php the_title(); ?></h1>
				<span class="post-by"><?php esc_html_e('By ', 'materialism') ?><?php the_author_link(); ?></span>
				<?php the_category(); ?>
			</div>
			<div class="single-share-block">
		    		<span class="circle-btn share-btn"></span>
		    		<?php tt_share(); ?>
		    	</div>
		</div>
	</section>

	<section class="single-blog-post">
		<div class="container <?php if($sidebar === 'right' && $sidebar_status) echo 'has-sidebar'; elseif ($sidebar === 'left') echo 'has-sidebar-left'; ?>">
			<?php if('full_width' !== $sidebar && $sidebar_status) : ?>
				<div class="row">
			<?php endif; ?>
				<?php if($sidebar === 'left' && $sidebar_status) : ?>
					<div class="col-md-4">
						<?php get_sidebar() ?>
					</div>
				<?php endif; ?>
				<div class="col-md-<?php if( $sidebar !== 'full_width' && $sidebar_status ) print '8'; else print '10 col-md-offset-1';  ?>">
					<div class="description">
						<?php the_content(); 
	                        $defaults = array(
	                            'before'           => '<p class="post-content-pag">',
	                            'after'            => '</p>',
	                            'link_before'      => '',
	                            'link_after'       => '',
	                            'next_or_number'   => 'next',
	                            'separator'        => '&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;',
	                            'nextpagelink'     => esc_attr_x( 'Next page','single','materialism').'<i class="fa fa-arrow-right"></i>',
	                            'previouspagelink' => '<i class="fa fa-arrow-left"></i> '.esc_attr_x( 'Previous page ','single','materialism' ),
	                            'pagelink'         => '%',
	                            'echo'             => 1
	                        );                                             
	                        wp_link_pages( $defaults );
	                    ?>
					</div>
					<div class="tag-list">
						<?php the_tags('',''); ?>
					</div>
					<?php comments_template(); ?>
				</div>
				<?php if($sidebar === 'right' && $sidebar_status) : ?>
					<div class="col-md-4">
						<?php get_sidebar() ?>
					</div>
				<?php endif; ?>
			<?php if('full_width' !== $sidebar && $sidebar_status) : ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endwhile; endif; 
if($related_posts_on_off === 'show_related_posts') :
	get_template_part( 'templates/relatedposts' ); 
endif; 

if($single_post_shortcodes) :
	$shortcodes_list = str_replace(', ', ',', $single_post_shortcodes); 
	$shortcodes = explode(",", $shortcodes_list); 
	foreach ($shortcodes as $shortcod) :
		print do_shortcode( $shortcod );
	endforeach;
endif;
 get_footer(); ?>