<?php get_header(); ?>

<section class="error-404" style="<?php if(_go('404_bg_image')) echo "background-image: url('". _go('404_bg_image')."'); background-size: cover;" ?>">
	<div class="centered-content">
		<h3 class="subtitle"><?php _go('404_title') ? _eo('404_title') : esc_html_e('Error', 'materialism'); ?></h3>
		<h1 class="title"><?php esc_html_e('404', 'materialism'); ?></h1>
		<p class="description"><?php _go('404_message') ? _eo('404_message') : esc_html_e('The Page you are looking for cannot be found', 'materialism'); ?></p>
		<a href="<?php echo esc_url( home_url('/') ) ?>" class="btn gradient big"><?php _go('404_button_text') ? _eo('404_button_text') : esc_html_e('Go Home','materialism'); ?></a>
	</div>
</section>

<?php get_footer();