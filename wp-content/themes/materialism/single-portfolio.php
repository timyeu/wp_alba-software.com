<?php get_header( );
$page_id = tt_get_page_id();
$related_posts_on_off = get_post_meta( $page_id, THEME_NAME . '_related_posts_on_off', true );
$single_post_shortcodes = get_post_meta( $page_id, THEME_NAME . '_single_post_shortcodes', true );

 if (have_posts()) :  while (have_posts()) : the_post(); 
	$terms = get_the_terms( get_the_ID(),'portfolio_tax');
	$options = get_post_meta(get_the_ID(), 'slide_options', true);
	$post_thumbnail_url = wp_get_attachment_url( $options['full_image'] );
 ?>
<section class="hero-section single portfolio" style="<?php if($post_thumbnail_url) echo "background: url('".$post_thumbnail_url."') center center;"; else echo "background: #242433;"; ?>">
	<div class="container">
		<div class="centered-content">
			<h1 class="post-title"><?php the_title() ?></h1>
			<span class="post-by"><?php esc_html_e('By ', 'materialism') ?><?php the_author_link(); ?></span>
		</div>

		<div class="single-share-block">
			<span class="circle-btn share-btn"></span>
			<?php tt_share(); ?>
		</div>
	</div>
</section>
<section class="single-blog-post portfolio">
	<div class="top-block">
		<div class="container-fluid max-container">
			<div class="row">
				<div class="col-md-6">
					<?php if(has_post_thumbnail()):?>
						<div class="cover">
							<?php the_post_thumbnail('tt_portfolio_single'); ?>
						</div>
					<?php endif; ?>

					<div class="recommend">
						<span class="likes <?php if( @$_COOKIE['tt_materialism_post_likes_'. $page_id]) echo esc_attr('liked'); else echo ''; ?>" data-id="<?php echo esc_attr($page_id);?>"><?php print get_post_meta($page_id, 'tt_materialism_post_likes', true) ? get_post_meta($page_id, 'tt_materialism_post_likes', true) : '0' ;?></span>
						<?php esc_html_e('Recommend','materialism'); ?>
					</div>
				</div>
				<div class="col-md-5">
					<div class="description">
						<?php if($options['description']) : ?>
							<h3><?php esc_html_e('Description','materialism'); ?></h3>
							<p><?php tt_print($options['description']) ?></p>
						<?php endif; ?>
						<?php if($options['quote']['quote_text']) : ?>
							<h3><?php esc_html_e('Quote','materialism'); ?></h3>
							<p><?php esc_html_e('“ ','materialism'); ?><?php tt_print($options['quote']['quote_text']) ?><?php esc_html_e(' ”','materialism'); ?><br/>
							<span><?php tt_print($options['quote']['quote_author']) ?></span></p>
						<?php endif; ?>
						<?php if(has_tag( )) : ?>
							<div class="tags">
								<h3><?php esc_html_e('Tags','materialism'); ?></h3>
								<?php the_tags( '', '', '' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php if($related_posts_on_off === 'show_related_posts') :
	get_template_part( 'templates/related_portfolios' ); 
endif; ?>
</section>

<?php endwhile; endif; 

if($single_post_shortcodes) :
	$shortcodes_list = str_replace(', ', ',', $single_post_shortcodes); 
	$shortcodes = explode(",", $shortcodes_list); 
	foreach ($shortcodes as $shortcod) :
		print do_shortcode( $shortcod );
	endforeach;
endif;
 get_footer(); ?>