(function ($) {
	'use strict';

	// Template Helper Function
	$.fn.hasAttr = function(attribute) {
		var obj = this;

		if (obj.attr(attribute) !== undefined) {
			return true;
		} else {
			return false;
		}
	};

	var Base64 = {
		_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
		encode: function(e) {
			var t = "";
			var n, r, i, s, o, u, a;
			var f = 0;
			e = Base64._utf8_encode(e);
			while (f < e.length) {
				n = e.charCodeAt(f++);
				r = e.charCodeAt(f++);
				i = e.charCodeAt(f++);
				s = n >> 2;
				o = (n & 3) << 4 | r >> 4;
				u = (r & 15) << 2 | i >> 6;
				a = i & 63;
				if (isNaN(r)) {
					u = a = 64
				} else if (isNaN(i)) {
					a = 64
				}
				t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
			}
			return t
		},
		decode: function(e) {
			var t = "";
			var n, r, i;
			var s, o, u, a;
			var f = 0;
			e = e.replace(/[^A-Za-z0-9+/=]/g, "");
			while (f < e.length) {
				s = this._keyStr.indexOf(e.charAt(f++));
				o = this._keyStr.indexOf(e.charAt(f++));
				u = this._keyStr.indexOf(e.charAt(f++));
				a = this._keyStr.indexOf(e.charAt(f++));
				n = s << 2 | o >> 4;
				r = (o & 15) << 4 | u >> 2;
				i = (u & 3) << 6 | a;
				t = t + String.fromCharCode(n);
				if (u != 64) {
					t = t + String.fromCharCode(r)
				}
				if (a != 64) {
					t = t + String.fromCharCode(i)
				}
			}
			t = Base64._utf8_decode(t);
			return t
		},
		_utf8_encode: function(e) {
			e = e.replace(/rn/g, "n");
			var t = "";
			for (var n = 0; n < e.length; n++) {
				var r = e.charCodeAt(n);
				if (r < 128) {
					t += String.fromCharCode(r)
				} else if (r > 127 && r < 2048) {
					t += String.fromCharCode(r >> 6 | 192);
					t += String.fromCharCode(r & 63 | 128)
				} else {
					t += String.fromCharCode(r >> 12 | 224);
					t += String.fromCharCode(r >> 6 & 63 | 128);
					t += String.fromCharCode(r & 63 | 128)
				}
			}
			return t
		},
		_utf8_decode: function(e) {
			var t = "";
			var n = 0;
			var r = 0;
			var c1 = 0;
			var c2 = 0;
			while (n < e.length) {
				r = e.charCodeAt(n);
				if (r < 128) {
					t += String.fromCharCode(r);
					n++
				} else if (r > 191 && r < 224) {
					c2 = e.charCodeAt(n + 1);
					t += String.fromCharCode((r & 31) << 6 | c2 & 63);
					n += 2
				} else {
					c2 = e.charCodeAt(n + 1);
					c3 = e.charCodeAt(n + 2);
					t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
					n += 3
				}
			}
			return t
		}
	}

	var teslaThemes = {

		statisticsCounterVisible: false,
		progressCounter: false,

		init: function () {
			this.checkVisible();
			this.statsCounter();
			this.stickyHeader();
			this.isotopeInit();
			this.carouselInit();
			this.toggles();
			this.googleMaps();
			this.anchorScroll();
		},

		checkVisible: function () {
			var $w = $(window);
			$.fn.visible = function(partial,hidden,direction){

				if (this.length < 1)
					return;

				var $t        = this.length > 1 ? this.eq(0) : this,
					t         = $t.get(0),
					vpWidth   = $w.width(),
					vpHeight  = $w.height(),
					direction = (direction) ? direction : 'both',
					clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;

				if (typeof t.getBoundingClientRect === 'function'){

					var rec = t.getBoundingClientRect(),
						tViz = rec.top    >= 0 && rec.top    <  vpHeight,
						bViz = rec.bottom >  0 && rec.bottom <= vpHeight,
						lViz = rec.left   >= 0 && rec.left   <  vpWidth,
						rViz = rec.right  >  0 && rec.right  <= vpWidth,
						vVisible   = partial ? tViz || bViz : tViz && bViz,
						hVisible   = partial ? lViz || rViz : lViz && rViz;

					if(direction === 'both')
						return clientSize && vVisible && hVisible;
					else if(direction === 'vertical')
						return clientSize && vVisible;
					else if(direction === 'horizontal')
						return clientSize && hVisible;
				} else {

					var viewTop         = $w.scrollTop(),
						viewBottom      = viewTop + vpHeight,
						viewLeft        = $w.scrollLeft(),
						viewRight       = viewLeft + vpWidth,
						offset          = $t.offset(),
						_top            = offset.top,
						_bottom         = _top + $t.height(),
						_left           = offset.left,
						_right          = _left + $t.width(),
						compareTop      = partial === true ? _bottom : _top,
						compareBottom   = partial === true ? _top : _bottom,
						compareLeft     = partial === true ? _right : _left,
						compareRight    = partial === true ? _left : _right;

					if(direction === 'both')
						return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
					else if(direction === 'vertical')
						return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
					else if(direction === 'horizontal')
						return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
				}
			};
		},

		statsCounter: function () {
			function runStats() {
				if ($('.stats-item').visible( true ) && !teslaThemes.statisticsCounterVisible) {
					teslaThemes.statisticsCounterVisible = true;
					$('.stats-item .number').each(function () {
						var $this = $(this);
						jQuery({ Counter: 0 }).animate({ Counter: parseInt($this.html(), 10) }, {
							duration: 3000,
							easing: 'swing',
							step: function (now) {
								$this.text(Math.ceil(now));
							}
						});
					});
				}
			
				if ($('.progress-item').visible( true ) && !teslaThemes.progressCounter) {
					teslaThemes.progressCounter = true;
					$('.progress-item .progress-bar').each(function () {
						var $this = $(this),
							check_val = $this.parent().hasClass('vertical');

						jQuery({ Counter: 0 }).animate({ Counter: parseInt($this.data('number'), 10) }, {
							duration: 3000,
							easing: 'swing',
							step: function (now) {
								$this.find('.heading .number').text(Math.ceil(now));

								if(!check_val)
									$this.width(Math.ceil(now) + '%');
								else
									$this.height(Math.ceil(now) + '%');
							}
						});
					});
				}	
			}

			runStats();

			$(document).scroll(function () {
				runStats();
			});
		},

		stickyHeader: function () {
			if ($('header').hasClass('sticky')) {
				$(window).on('scroll', function () {
					var st = $(this).scrollTop();

					if (st > $('header').outerHeight(true) + 70) {
						$('header').addClass('fixed');
					} else {
						$('header').removeClass('fixed');
					}
				});
			}
		},

		isotopeInit: function () {
			var isotopeContainer = $('.isotope-container'),
				defaultSelection = $('.isotope-container').attr('data-default-selection');
			
			isotopeContainer.imagesLoaded(function () {
				isotopeContainer.isotope({
					filter: defaultSelection,
					itemSelector: '.isotope-item',
					hiddenStyle: {
					  opacity: 0,
					  transform: 'scale(0.001)'
					},
					visibleStyle: {
					  opacity: 1,
					  transform: 'scale(1)'
					},
					transitionDuration: '0.8s'
				});
			});

			$('.isotope-filters a').on('click', function () {
				$('.isotope-filters .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
					isotopeContainer.isotope({
						filter: selector
					});
				return false;
			});
		},

		carouselInit: function () {
			var i = $(".slick-carousel");

			i.each(function() {
				var e = $(this),
					a = e.find(".carousel-items");
				a.slick({
					focusOnSelect: !0,
					speed: e.hasAttr("data-speed") ? e.data("speed") : 600,
					slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-desktop") : 4,
					arrows: e.hasAttr("data-arrows") ? e.data("arrows") : !0,
					appendArrows: e, 
					dots: e.hasAttr("data-dots") ? e.data("dots") : !0,
					infinite: e.hasAttr("data-infinite") ? e.data("infinite") : !1,
					slidesToScroll: e.hasAttr("data-items-to-slide") ? e.data("items-to-slide") : 1,
					initialSlide: e.hasAttr("data-start") ? e.data("start") : 0,
					asNavFor: e.hasAttr("data-as-nav-for") ? e.data("as-nav-for") : "",
					centerMode: e.hasAttr("data-center-mode") ? e.data("center-mode") : "",
					fade: e.hasAttr("data-fade") ? e.data("fade") : false,
					easing: e.hasAttr("data-easing") ? e.data("easing") : "linear",
					responsive: [
						{
						  breakpoint: 992,
						  settings: {
							slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-tablet") : 2,
							slidesToScroll: e.hasAttr("data-items-desktop") ? e.data("items-tablet") : 2,
							infinite: true,
						  }
						},
						{
						  breakpoint: 768,
						  settings: {
							slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-mobile") : 1,
							slidesToScroll: e.hasAttr("data-items-desktop") ? e.data("items-mobile") : 1
						  }
						},
						{
						  breakpoint: 620,
						  settings: {
							slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-xs-mobile") : 1,
							slidesToScroll: e.hasAttr("data-items-desktop") ? e.data("items-xs-mobile") : 1
						  }
						}
					]
				})
			})
		},

		toggles: function () {
			// Search toggle
			$('header .search-toggle').on('click', function () {
				$('html').addClass('search-form-visible');
				$('.header-search-form .input-line').focus();
			});

			$('.header-search-form .close-icon').on('click', function () {
				$('html').removeClass('search-form-visible');
			});

			// Video Popup
			$('.video-toggle, .video-toggle-s').on('click', function () {
				var video_embed = $(this).data('embed');
				$('.video-popup .responsive-media').html('<iframe src="' + video_embed + '" allowfullscreen >');
				$('html').addClass('video-popup-visible');
				return false;
			});

			$('.video-popup .media-wrapper').on('click', function (e) {
				e.stopPropagation();
			});

			$('html, .close-video-popup-toggle').on('click', function () {
				$('html').removeClass('video-popup-visible');
				var media = $('.media-wrapper');
				media.html(media.html());
			});

			// Mobile Nav Toggle
			$('.mobile-toggle').on('click', function () {
				$('body').toggleClass('mobile-nav-visibile');
				return false;
			});

			$('header .main-nav').on('click', function (e) {
				if ($(window).width() < 1200) {
					e.stopPropagation();
				}
			});

			$(document).on('click', function () {
				$('body').removeClass('mobile-nav-visibile');
			});

			// Mobile Submenus
			$('header .main-nav li.menu-item-has-children > a').on('click', function (e) {
				var obj = $(this);

				if ($(window).width() < 1200) {
					e.preventDefault();
					obj.parent().toggleClass('active');
					obj.next().slideToggle(250);
				}
			});

			// Next Section
			$('.hero-section .go-down').on('click', function () {
				var index = $(this).closest('.hero-section').index();
				$("body").animate({
					 scrollTop: $('.content-wrapper').find('section').eq(index + 1).offset().top + 700
				}, 900);
			});

			// Tabbed Content
			var tabbed = $('.tabbed-content');

			tabbed.each(function (i, val){
				$(this).find('.tabs li').on('click', function(){
					var tab_id = $(this).attr('data-tab');

					$(val).find('.tabs li').removeClass('current');
					$(val).find('.tab-content').fadeOut(0).removeClass('current');

					$(this).fadeIn(800).addClass('current');
					$(val).find("#"+tab_id).fadeIn(800).addClass('current');
				});
			});

			// Check Value
			$('.contact-form .input-line .label').on('click', function(){
				$(this).parent().find('.check-value').focus()
			});

			var required_field = $('.contact-form .input-line.required');
			var validate_email = function validateEmail(email) {
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(email);
			};

			function check_val(elem) {
				var text_val = elem.val(),
					type = elem.attr('type');

				if (text_val === "" || text_val.replace(/^\s+|\s+$/g, '') === "")
					elem.parent().removeClass('has-value');
				else	
					elem.parent().addClass('has-value');

				if( (type == "email" && !validate_email(text_val)) )
					elem.parent().addClass('no-email');
				else
					elem.parent().removeClass('no-email');
				
			}

			$('.check-value').each(function(){
				check_val($(this));
			});

			$('.check-value').on('keyup', function () {
				var i = 0;
				check_val($(this));
				required_field.each(function(){
					if($(this).hasClass('has-value'))
						i++;

					if($(this).hasClass('no-email'))
						i--;

					if(i == required_field.length)
						$('.contact-form .input-line.submit').addClass('has-value');
					else 
						$('.contact-form .input-line.submit').removeClass('has-value');
				});
			});

		// Post Likes
			$('div .likes').on('click', function(){
				var box = $(this);
				var post_id = box.data('id');
				var likes = parseInt(box.text(),10);
				var likesCount = $( '.recommend .likes' );
				
				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data: {action: 'post_likes', postid:post_id},
				})
				.done(function(result) {
					if(result == true) {
						if(box.hasClass('liked')){
							likes--;
							box.removeClass('liked');
							likesCount.text(likes);
						} else {	
							likes++;
							likesCount.text(likes);
							box.addClass('liked')
						}
					}
				})
				.fail(function() {
					console.log("error-ajax");
				});
			});
		},

		googleMaps: function () {
			function initialize_contact_map (customOptions) {
				var mapOptions = {
						center: new google.maps.LatLng(customOptions.map_center.lat, customOptions.map_center.lon),
						zoom: parseInt(customOptions.zoom),
						scrollwheel: false,
						disableDefaultUI: true,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						styles: [{ stylers: [{saturation: 20 }]}]
					};
				var contact_map = new google.maps.Map($('#map-canvas')[0], mapOptions),
					marker = new google.maps.Marker({
						map: contact_map,
						position: new google.maps.LatLng(customOptions.marker_coord.lat, customOptions.marker_coord.lon),
						animation: google.maps.Animation.DROP,
						icon: customOptions.marker,
					});
			}

			if ($('#map-canvas').length) {
				var customOptions = $('#map-canvas').data('options');

				setTimeout(function(){
					google.maps.event.addDomListener(window, 'load', initialize_contact_map(customOptions));
				}, 1000);
			}
		},

		anchorScroll: function () {
			$('.menu-item > a').bind('click.smoothscroll',function (e) {
				var target = this.hash,
				$target = $(target);

				if(target && $(target).length > 0)
					 e.preventDefault();
			  
				if($target.offset()) {
					$('html, body').stop().animate({
						'scrollTop': $target.offset().top-$('header').height()
					}, 1200, 'swing');
				}
				if(!$(this).parent().hasClass('menu-item-has-children'))
				$('body').removeClass('mobile-nav-visibile');
			});

			var last_index,
				sections_nr = $('.section').length + 1;

			$(window).on('scroll', function(){
				$('.section').each(function (i, val) {
					if($(val).visible(true)) {

						if($(val).index() == sections_nr && $(val).outerHeight() < 500)
							last_index = $(val).outerHeight() + 150;
						else 
							last_index = 0;

						var distance = $(val).offset().top,
						$window = $(window);

						if ( $window.scrollTop() + $('header').height() + 100 + last_index >= distance ) {
							var container = $(val).attr('id');
							$('.menu-item a').each(function (i, val) {
								if($(val).attr('href').indexOf('#' + container) != -1) {
									$('.menu-item a').parents('.menu-item').removeClass('active');
									$(this).parents('.menu-item').addClass('active');
								}
								else {
									$(this).parents('.menu-item').removeClass('active');
								}
							});
						}
					}
				});
			});
		},
	};

	$(document).ready(function(){
		teslaThemes.init();

		setTimeout(function(){
			$('body').addClass('dom-ready');
		}, 300);
	});
}(jQuery));