<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<?php wp_head() ?>
	</head>

<body id="page" <?php body_class(); ?>>
	<!-- Video Popup -->
	<div class="popup-wrapper video-popup">
		<!-- Close Video Popup -->
		<span class="close-video-popup-toggle"></span>
		
		<div class="popup-inner-content">
			<div class="media-wrapper">
				<div class="responsive-media ratio16by9"></div>
			</div>
		</div>
	</div>

	<div class="page-wrapper">
		<header class="<?php if(_go('navigation_style') === 'Sticky') echo 'sticky'; ?>">
			<form class="header-search-form" method="get" id="searchform" action="<?php echo esc_url( home_url('/') ) ?>">
				<input class="input-line" placeholder="<?php esc_attr_e('Search','materialism') ?>" value="<?php the_search_query(); ?>" name="s" id="s">
				<i class="icon-close-icon close-icon"></i>
			</form>
			
			<div class="container-fluid">
				<?php get_template_part( '/templates/logo' ); ?>
				<nav class="main-nav">
					<?php 
					   wp_nav_menu( 
						  array(
							 'theme_location' => 'primary',
							 'container' => false,
							 'menu_class' => false
						  )
					   );
					?>
				</nav>

				<div class="toggle-wrapper">
					<span class="mobile-toggle"></span>
						<div class="share-block">
							<i class="icon-share icon-mobile-menu"></i>
							<ul class="social-platforms clean-list">
								<?php $social_platforms = array('facebook','twitter','google','pinterest','linkedin','dribbble','behance','youtube','flickr','rss','instagram');
								foreach($social_platforms as $platform) :
									if (_go('social_platforms_'. $platform)): ?>
										<li><a href="<?php echo _go('social_platforms_' . $platform) ?>" target="_blank"><?php echo esc_attr( $platform ) ?></a></li>
									<?php endif;
								endforeach ?>
							</ul>
						</div>
					<i class="search-toggle icon-search"></i>
				</div>
			</div>
		</header>

		<div class="content-wrapper">